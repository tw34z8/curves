/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client.gameStates;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.effects.Effect.EffectEvent;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;
import com.jme3.system.AppSettings;
import com.jme3.texture.FrameBuffer;
import com.jme3.texture.Image.Format;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import curves.client.ClientMain;

import curves.client.GameUpdater;
import curves.common.Log;
import curves.common.Movement;

import static curves.common.Movement.applyDirectionUpdate;
import static curves.common.Movement.tick;
import curves.common.network.Objects.GameObject;
import curves.common.network.Objects.Player;
import curves.common.network.messages.game.DirectionUpdate;
import curves.common.network.messages.game.GameFinishedMessage;
import curves.common.network.messages.game.Snapshot;

/**
 *
 * @author adolph
 */
public class MainGameOnline extends GameState implements ActionListener {

	private Snapshot[] snapshotBuffer = new Snapshot[2];
	private String gameID;
	private final ArrayList<Player> players;
	private ArrayList<Player> localPlayers = new ArrayList<>();
	private Texture controllButtonTexture = getScreen().createNewTexture("Textures/buttons.png"); // Button texture map
	private Map<Player, List<DirectionUpdate>> unacknowledegedDirectionUpdates;
	private Node offNode;
	private ViewPort offView;
	private Texture offTex;
	public static BitmapText debugText;
	private Geometry offBox; //black background, if we dont do that initialy we see some weird stuff from the buffer (just on android)
	private GameUpdater gameUpdater;
	private double[] predictionDebugValues = new double[2];
	private double tick = 0;

	MainGameOnline(AppSettings settings, String gameID, ArrayList<Player> players, long startTime) {
		this.settings = settings;
		this.gameID = gameID;
		this.players = players;
	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {

		super.initialize(stateManager, app);
		// setup main scene
		Geometry quad = new Geometry("box", new Quad(settings.getWidth(), settings.getHeight()));

		offTex = setupOffscreenView();


		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setTexture("ColorMap", offTex);
		mat.getAdditionalRenderState().setBlendMode(BlendMode.Color);
		//quad.setQueueBucket(Bucket.Transparent);
		quad.setMaterial(mat);
		quad.setLocalTranslation(new Vector3f(0, 0, -1000));
		guiNodeAttachChild(quad);

		unacknowledegedDirectionUpdates = new HashMap<Player, List<DirectionUpdate>>();


		gameUpdater = new GameUpdater(players, assetManager, offNode, settings, snapshotBuffer, unacknowledegedDirectionUpdates, clientMain, gameID);
		// setup players
		for (int i = 0; i < players.size(); i++) {
			final Player player = players.get(i);
			if (player.getConnectionID().equals(clientMain.getConnectionId())) { // lokal player
				localPlayers.add(player);
				unacknowledegedDirectionUpdates.put(player, new CopyOnWriteArrayList<DirectionUpdate>());
			}
		}

		switch (localPlayers.size()) {
			case 1:

				if (isAndroid()) {
					createPlayerControlsAndroid(localPlayers.get(0), 15);
				} else {
					inputManager.addMapping("left", new KeyTrigger(KeyInput.KEY_A));
					inputManager.addMapping("right", new KeyTrigger(KeyInput.KEY_D));
					inputManager.addListener(this, "left", "right");
				}
				break;
			case 2:
				if (isAndroid()) {
					createPlayerControlsAndroid(localPlayers.get(0), 15);
					createPlayerControlsAndroid(localPlayers.get(1), settings.getWidth() - 95);
				} else {
					inputManager.addMapping("left", new KeyTrigger(KeyInput.KEY_A));
					inputManager.addMapping("right", new KeyTrigger(KeyInput.KEY_D));
					inputManager.addMapping("left2", new KeyTrigger(KeyInput.KEY_LEFT));
					inputManager.addMapping("right2", new KeyTrigger(KeyInput.KEY_RIGHT));

					inputManager.addListener(this, "left", "right", "left2", "right2");
				}
				break;

		}

		app.getCamera().setLocation(new Vector3f(3, 3, 3));
		app.getCamera().lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);

		BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		debugText = new BitmapText(guiFont, false);
		debugText.setSize(guiFont.getCharSet().getRenderedSize());
		debugText.setLocalTranslation(100, settings.getHeight() - debugText.getLineHeight(), 0);
		debugText.setText("DEBUG");

		guiNodeAttachChild(debugText);
		clientMain.getSound().setBackgoundMusicGame();

	}

	private void createPlayerControlsAndroid(final Player player, int xOffset) {

		ButtonAdapter buttonAdapter = new ButtonAdapter(getScreen(), UUID.randomUUID().toString(), new Vector2f(xOffset, 15), new Vector2f(89, 209)) {
			@Override
			public void onButtonMouseLeftDown(MouseButtonEvent evt, boolean toggled) {
				player.setKey_left(true);
			}

			@Override
			public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
				player.setKey_left(false);
			}
		};
		buttonAdapter.setText("<"); // TODO better symbol
		buttonAdapter.setTextureAtlasImage(controllButtonTexture, "x=0|y=0|w=89|h=208");
		buttonAdapter.removeEffect(EffectEvent.Hover);
		buttonAdapter.removeEffect(EffectEvent.GetFocus);
		buttonAdapter.setButtonPressedInfo("x=90|y=0|w=89|h=209", ColorRGBA.White);
		buttonAdapter.getMaterial().setColor("Color", player.getColor());
		addScreenElement(buttonAdapter);

		ButtonAdapter buttonAdapter2 = new ButtonAdapter(getScreen(), UUID.randomUUID().toString(), new Vector2f(xOffset, settings.getHeight() - 215),
			new Vector2f(89, 209)) {
			@Override
			public void onButtonMouseLeftDown(MouseButtonEvent evt, boolean toggled) {
				player.setKey_right(true);
			}

			@Override
			public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
				player.setKey_right(false);
			}
		};
		buttonAdapter2.setText(">");// TODO better symbol
		buttonAdapter2.setTextureAtlasImage(controllButtonTexture, "x=0|y=0|w=89|h=209");
		buttonAdapter2.removeEffect(EffectEvent.Hover);
		buttonAdapter2.removeEffect(EffectEvent.GetFocus);
		buttonAdapter2.setButtonPressedInfo("x=90|y=0|w=89|h=209", ColorRGBA.White);
		buttonAdapter2.getMaterial().setColor("Color", player.getColor());
		addScreenElement(buttonAdapter2);

	}

	// Note that update is only called while the state is both attached and
	// enabled.
	@Override
	public void update(final float tpf) {
		if (tick == 1) {
			offBox.removeFromParent(); //echt nicht nice
		}
		gameUpdater.updateWorld(tpf);
		offNode.updateGeometricState();
		tick++;
	}

	public Texture setupOffscreenView() {

		Camera offCamera = new Camera(settings.getWidth(), settings.getHeight());
		offNode = new Node("offNode");

		offView = clientMain.getRenderManager().createPreView("Offscreen View", offCamera);
		offView.setClearFlags(false, false, false);
		offView.setBackgroundColor(ColorRGBA.DarkGray);
		// create offscreen framebuffer
		FrameBuffer offBuffer = new FrameBuffer(settings.getWidth(), settings.getHeight(), 1);

		// setup framebuffer's cam
		int right = settings.getWidth() / 2;
		int left = -right;
		int top = settings.getHeight() / 2;
		int bottom = -settings.getHeight() / 2;
		offCamera.setFrustum(-1000, 1000f, left, right, top, bottom);
		offCamera.setParallelProjection(true);

		// setup framebuffer's texture
		Texture2D offTex = new Texture2D(settings.getWidth(), settings.getHeight(), Format.RGBA8);
		offBuffer.setColorTexture(offTex);

		// set viewport to render to offscreen framebuffer
		offView.setOutputFrameBuffer(offBuffer);
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md"); // create
		// a
		// simple
		// material
		// setup framebuffer's scene

		Quad boxMesh = new Quad(settings.getWidth(), settings.getHeight());
		mat.setColor("Color", new ColorRGBA(0f, 0f, 0f, 1f)); // set color of material to blue
		offBox = new Geometry("box", boxMesh);
		offBox.setMaterial(mat);
		offBox.rotate(0, 180 * FastMath.DEG_TO_RAD, 0);
		offBox.setLocalTranslation(settings.getWidth() / 2, -settings.getHeight() / 2, -0);
		offNode.attachChild(offBox);

		// attach the scene to the viewport to be rendered

		offView.attachScene(offNode);

		return offTex;
	}

	@Override
	public void snapshotReceived(Snapshot snapshot) {

		snapshotBuffer[0] = snapshotBuffer[1];
		snapshotBuffer[1] = snapshot;
		if (gameUpdater == null) {
			return;
		}

		for (Player player : players) {
			final List<DirectionUpdate> directionUpdateList = unacknowledegedDirectionUpdates.get(player);

			if (directionUpdateList == null || !this.isInitialized()) {
				continue;
			}
			Player playerSnapshot = new Player();
			for (GameObject p1 : snapshot.getGameObjects()) {
				if (p1.getID().equals(player.getID())) {
					playerSnapshot = (Player) p1;
				}
			}
			//remove all acknowledged updates
			for (DirectionUpdate directionUpdate : directionUpdateList) {
				if (directionUpdate.getDirectionUpdateId() <= playerSnapshot.getLastAcknowlegedDirectionUpdateId()) {
					directionUpdateList.remove(directionUpdate);
					//Log.debug("Removing directionUpdate with id '%s'", directionUpdate.getDirectionUpdateId());
				}
			}
			Player dummy = new Player();
			dummy.setPosition(playerSnapshot.getPosition().clone());
			dummy.setDirectionVector(playerSnapshot.getDirectionVector().clone());
			dummy.setDirection(playerSnapshot.getDirection());

			//reconcilation
			for (DirectionUpdate directionUpdate : directionUpdateList) {
				Movement.applyDirectionUpdate(dummy, directionUpdate);
			}

			//debugstuff-start
			float r = dummy.getDirectionVector().x / dummy.getDirectionVector().y;
			float r2 = player.getDirectionVector().x / player.getDirectionVector().y;
			//Log.debug("x1: " + dummy.getDirectionVector().x + " x2: " + player.getDirectionVector().x + " y1: " + dummy.getDirectionVector().y + " y2: " + player.getDirectionVector().y + " r: " + r + " r2: " + r2);
//			debugText.setText("Prediction: " + ((int) ((predictionDebugValues[0] / (predictionDebugValues[0] + predictionDebugValues[1])) * 100)) + "%, unacknowledged inputs: " + directionUpdateList.size());
			if (!(Float.isInfinite(r) || Float.isInfinite(r2))) {
				if (Math.abs(r - r2) < 0.1) {
					//debugText.setColor(ColorRGBA.Green);
					predictionDebugValues[0]++;
				} else {
					//debugText.setColor(ColorRGBA.Red);
					predictionDebugValues[1]++;
				}
			} else {
				if (Float.isInfinite(r) && Float.isInfinite(r2)) {
					//debugText.setColor(ColorRGBA.Green);
					predictionDebugValues[0]++;
				}

			}
			//debugstuff-end

			player.setDirectionVector(dummy.getDirectionVector());
			player.setPosition(dummy.getPosition());
			player.setDead(playerSnapshot.isDead());

		}
		gameUpdater.setLocalTime(snapshot.getTime());
	}

	@Override
	public void gameFinished(GameFinishedMessage gameFinishedMessage) {
		Log.info("The game was finished. The winner is '%s'", gameFinishedMessage.getWinner().getUserName());

		changeState(new GameFinishedGameState(settings, gameFinishedMessage));

	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {
		if (name.equals("right") && isPressed) {
			localPlayers.get(0).setKey_right(true);

		} else if (name.equals("right") && !isPressed) {
			localPlayers.get(0).setKey_right(false);

		}

		if (name.equals("left") && isPressed) {
			localPlayers.get(0).setKey_left(true);

		} else if (name.equals("left") && !isPressed) {
			localPlayers.get(0).setKey_left(false);
		}

		if (name.equals("right2") && isPressed) {
			localPlayers.get(1).setKey_right(true);
		} else if (name.equals("right2") && !isPressed) {
			localPlayers.get(1).setKey_right(false);
		}

		if (name.equals("left2") && isPressed) {
			localPlayers.get(1).setKey_left(true);
		} else if (name.equals("left2") && !isPressed) {
			localPlayers.get(1).setKey_left(false);
		}
	}
}
