/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client.gameStates;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.lists.SelectList;
import tonegod.gui.controls.text.Label;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;

import curves.common.Log;
import curves.common.network.Objects.Player;
import curves.common.network.Room;
import curves.common.network.messages.LoginResponse;
import curves.common.network.messages.game.StartGame;
import curves.common.network.messages.game.StartGameBroadcast;

/**
 * State were users wait in room till game begins
 *
 * @author Death
 *
 */
public class RoomGameState extends GameState {

	private Label roomLabel;
	private SelectList playerList;
	private Button startGameButton;
	private Button cancelButton;
	private Button addPlayerButton;

	private Room currentRoom;
	private ArrayList<Player> players = new ArrayList<>(); // just for logout in case the user cancels

	public RoomGameState(Room room, AppSettings settings) {
		this.settings = settings;
		currentRoom = room;
	}

	public void startGameButtonClick(MouseButtonEvent evt, boolean toggled) {
		Log.info("Client wants to start game.");
		clientMain.getConnection().startGame(new StartGame(currentRoom));
		// changeState(new MainGame(settings)); // start game
	}

	public void cancelButtonClick(MouseButtonEvent evt, boolean toggled) {
		cancel();
	}

	public void addPlayerButtonClick(MouseButtonEvent evt, boolean toggled) {
		Log.info("Client wants to add another player on the same device.");
		clientMain.getConnection().login(null);
	}

	@Override
	public void initialize(final AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);

		getScreen().parseLayout("Interface/Room.gui.xml", this);

		roomLabel = (Label) getScreen().getElementById("roomLabel");
		textSize(roomLabel);

		playerList = (SelectList) getScreen().getElementById("playerList");
		startGameButton = (Button) getScreen().getElementById("startGameButton");
		textSize(getScreen().getElementById("startGameButtonText"));
		cancelButton = (Button) getScreen().getElementById("cancelButtonRoom");
		textSize(getScreen().getElementById("cancelButtonRoomText"));
		addPlayerButton = (Button) getScreen().getElementById("addPlayerButton");
		textSize(getScreen().getElementById("addPlayerButtonText"));

		updatePlayerList(currentRoom);
	}

	@Override
	public void roomUpdated(Room room) {
		currentRoom = room;
		updatePlayerList(room);
	}

	private void updatePlayerList(final Room room) {
		Log.info("A room update was received.");
		currentRoom = room;

		// Update playerList
		clientMain.enqueue(new Callable<Spatial>() {
			@Override
			public Spatial call() throws Exception {
				playerList.removeAllListItems();

				for (Player player : room.getPlayers()) {
					String userName = player.getUserName();
					playerList.addListItem(userName, null);
				}
				return playerList;
			}
		});
		// Update room text
		clientMain.enqueue(new Callable<Spatial>() {
			@Override
			public Spatial call() throws Exception {
				roomLabel.setText("Room " + room.getId() + "(" + room.getPlayerCount() + "/" + room.getMaxPlayers() + ")");
				return roomLabel;
			}
		});

		// Check if admin and more than 2 players are in room
		clientMain.enqueue(new Callable<Spatial>() {
			@Override
			public Spatial call() throws Exception {
				ArrayList<Player> players = room.getPlayers();
				if (players.size() > 1 && players.get(0).getConnectionID().equals(clientMain.getConnectionId())) {
					startGameButton.setIsEnabled(true);
				} else {
					startGameButton.setIsEnabled(false);
				}

				return startGameButton;
			}
		});
	}

	@Override
	public void gameStarted(StartGameBroadcast startGameBroadcast) {
		final int ping = 0; // TODO use real ping
		final long currentTimeMillis = System.currentTimeMillis() - ping / 2;
		changeState(new MainGameOnline(settings, startGameBroadcast.getGameID(), startGameBroadcast.getPlayers(), currentTimeMillis));
	}

	@Override
	public void loginResponse(LoginResponse loginResponse) {
		if (loginResponse.isSuccess()) {
			Log.debug("Succesfully logged in as '%s':'%s'", loginResponse.getPlayer().getUserName(), loginResponse.getPlayer().getConnectionID());
			clientMain.getConnection().joinRoom(currentRoom, loginResponse.getPlayer());
			players.add(loginResponse.getPlayer());
		} else {
			Log.error("Could not log in: %s", loginResponse.getMsg());
		}
	}

	private void cancel() {
		Log.info("Client wants to leave the room.");
		clientMain.getConnection().leaveRoom(currentRoom);
		for (Player player : players) {
			clientMain.getConnection().logout(player);
		}
		changeState(new OnlineGameMenu(settings, clientMain.getMainPlayer())); // return to lobby
	}
}
