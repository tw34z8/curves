package curves.client.gameStates;

import java.util.concurrent.Callable;

import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.text.TextField;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;

import curves.common.Log;
import curves.common.network.messages.LoginResponse;
import curves.server.ServerMain;

public class MainMenu extends GameState {

	private Label welcomeLabel;
	private Button onlineButton;
	private Button offlineButton;
	private Label playerNameLabel;
	private TextField playerName;
	private ButtonAdapter buttonAdapter;

	public MainMenu(AppSettings settings) {
		this.settings = settings;
	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);

		getScreen().parseLayout("Interface/MainMenu.gui.xml", this);

		welcomeLabel = (Label) getScreen().getElementById("WelcomeLabel");
		textSize(welcomeLabel);
		onlineButton = (Button) getScreen().getElementById("OnlineButton");
		textSize(getScreen().getElementById("OnlineButtonText"));
		offlineButton = (Button) getScreen().getElementById("OfflineButton");
		textSize(getScreen().getElementById("OfflineButtonText"));
		playerNameLabel = (Label) getScreen().getElementById("PlayerNameLabel");
		textSize(playerNameLabel);
		playerName = (TextField) getScreen().getElementById("PlayerName");
		textSize(playerName);

	}

	public void onlineButtonClick(MouseButtonEvent evt, boolean toggled) {
		String userName = playerName.getText();
		Log.debug("Trying to login as '%s'.", userName);
		clientMain.getConnection().login(userName);
	}

	public void offlineButtonClick(MouseButtonEvent evt, boolean toggled) {
		// start server on localhost
		System.out.println("Starting server");
		AppSettings settings = new AppSettings(true);
		settings.setFrameRate(100);
		ServerMain app2 = new ServerMain();
		app2.simpleInitApp();
		app2.setRootNode(rootNode);

		clientMain.offlineConnectionStuff();
		clientMain.getConnection().addConnectionListener(this);
		clientMain.getConnection().login(null);

		// changeState(new); // go to offline menu
	}

	@Override
	public void loginResponse(LoginResponse loginResponse) {
		if (loginResponse.isSuccess()) {
			Log.debug("Succesfully logged in as '%s':'%s'", loginResponse.getPlayer().getUserName(), loginResponse.getPlayer().getConnectionID());
			clientMain.setConnectionId(loginResponse.getPlayer().getConnectionID());
			changeState(new OnlineGameMenu(settings, loginResponse.getPlayer())); // go to online menu
			clientMain.setMainPlayer(loginResponse.getPlayer());
		} else {
			Log.error("Could not log in: %s", loginResponse.getMsg());
		}
	}

	@Override
	public void connectedToServer() {
		clientMain.enqueue(new Callable<Spatial>() {
			@Override
			public Spatial call() throws Exception {
				onlineButton.setIsEnabled(true);
				return onlineButton;
			}
		});

	}

	@Override
	public void disconnectedFromServer() {
		clientMain.enqueue(new Callable<Spatial>() {
			@Override
			public Spatial call() throws Exception {
				onlineButton.setIsEnabled(false);
				return onlineButton;
			}
		});
	}
}
