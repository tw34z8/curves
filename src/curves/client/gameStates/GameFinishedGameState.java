/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client.gameStates;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.text.Label;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont.Align;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.ColorRGBA;
import com.jme3.system.AppSettings;

import curves.common.network.Objects.Player;
import curves.common.network.messages.game.GameFinishedMessage;

/**
 *
 * @author adolph
 */
public class GameFinishedGameState extends GameState {

	private Button okButton;
	private GameFinishedMessage gameFinishedMessage;
	private Label winnerLabel;
	private Label[] playerLabel = new Label[4];

	public GameFinishedGameState(AppSettings settings, GameFinishedMessage gameFinishedMessage) {
		this.settings = settings;
		this.gameFinishedMessage = gameFinishedMessage;

	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);

		getScreen().parseLayout("Interface/GameFinished.gui.xml", this);

		winnerLabel = (Label) getScreen().getElementById("winnerLabel");
		textSize(winnerLabel);

		playerLabel[0] = (Label) getScreen().getElementById("p1Label");
		textSize(playerLabel[0]);
		playerLabel[1] = (Label) getScreen().getElementById("p2Label");
		textSize(playerLabel[1]);
		playerLabel[2] = (Label) getScreen().getElementById("p3Label");
		textSize(playerLabel[2]);
		playerLabel[3] = (Label) getScreen().getElementById("p4Label");
		textSize(playerLabel[3]);
		for (Label label : playerLabel) {
			label.setIsVisible(false);
		}
		okButton = (Button) getScreen().getElementById("okButton");
		textSize(getScreen().getElementById("okButtonText"));

		updateList();
		clientMain.getSound().setBackgoundMusicFinished();
	}

	public void okButtonClick(MouseButtonEvent evt, boolean toggled) {
		changeState(new OnlineGameMenu(settings, clientMain.getMainPlayer()));
	}

	private void updateList() {
		String winnerName = gameFinishedMessage.getWinner().getUserName();
		boolean localPlayerWon = false;
		if (gameFinishedMessage.getWinner().getConnectionID().equals(clientMain.getConnectionId())) {
			localPlayerWon = true;
		}

		// arrange buttons
		float buttonWidth = okButton.getWidth();
		int width = settings.getWidth();
		int height = settings.getHeight();
		int centerWidth = width / 2;

		winnerLabel.setText("Congratulation " + winnerName + "!!");

		List<Player> players = gameFinishedMessage.getPlayers();
		// Sort players
		Collections.sort(players, new Comparator<Player>() {
			@Override
			public int compare(Player arg0, Player arg1) {
				if (arg0.getPoints() < arg1.getPoints()) {
					return 1;
				}
				if (arg0.getPoints() > arg1.getPoints()) {
					return -1;
				}
				return 0;
			}
		});

		for (int i = 0; i < players.size(); i++) {
			Player player = players.get(i);
			playerLabel[i].setText(i + 1 + ". " + player.getUserName() + (player.isDisconnected() ? " (disconnected)" : ""));
			playerLabel[i].setTextAlign(Align.Center);
			// if player is winner, make bg white, else toggle
			if (gameFinishedMessage.getWinner().getID().equals(player.getID())) {
				playerLabel[i].getMaterial().setColor("Color", ColorRGBA.DarkGray);
			}
			playerLabel[i].setFontColor(player.getColor());
			playerLabel[i].setIsVisible(true);
		}

	}
}
