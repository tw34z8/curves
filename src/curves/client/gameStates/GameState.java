package curves.client.gameStates;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import tonegod.gui.controls.windows.AlertBox;
import tonegod.gui.core.Element;
import tonegod.gui.core.Screen;
import tonegod.gui.core.utils.BitmapTextUtil;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.jme3.system.AppSettings;

import curves.client.ClientMain;
import curves.client.network.ConnectionListener;
import curves.common.Log;
import curves.common.network.Objects.Player;
import curves.common.network.Room;
import curves.common.network.messages.LoginResponse;
import curves.common.network.messages.game.GameFinishedMessage;
import curves.common.network.messages.game.Snapshot;
import curves.common.network.messages.game.StartGameBroadcast;

public abstract class GameState extends AbstractAppState implements ConnectionListener {


	protected ClientMain clientMain;
	protected Node rootNode;
	private Node guiNode;
	protected AssetManager assetManager;
	protected AppStateManager stateManager;
	protected InputManager inputManager;
	protected ViewPort viewPort;
	protected AppSettings settings;
	protected BulletAppState physics;
	private static Screen screen = null;
	private List<Element> screenElements = new ArrayList<>();
	private List<Spatial> guiNodeSpatials = new ArrayList<>();
	private List<Control> guiNodeControls = new ArrayList<>();
        
        
	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		clientMain = (ClientMain) app;
		rootNode = clientMain.getRootNode();
		assetManager = clientMain.getAssetManager();
		this.stateManager = clientMain.getStateManager();
		inputManager = clientMain.getInputManager();
		viewPort = clientMain.getViewPort();
		guiNode = clientMain.getGuiNode();
		physics = this.stateManager.getState(BulletAppState.class);

		clientMain.getConnection().addConnectionListener(this);

		if (screen == null) {
			createScreen();
		}
               
	}

	/**
	 * Resizes the text in the given element to fit in it's bounds
	 *
	 * @param ref
	 */
	void textSize(Element ref) {
		float size = 10;
		ref.setFontSize(size);
		String text = ref.getText();
		float textWidth = BitmapTextUtil.getTextWidth(ref, text);
		float textHeight = BitmapTextUtil.getTextLineHeight(ref, text);
		while (textWidth < ref.getWidth() && textHeight < ref.getHeight()) {
			size += 1;
			ref.setFontSize(size);
			textWidth = BitmapTextUtil.getTextWidth(ref, text);
			textHeight = BitmapTextUtil.getTextLineHeight(ref, text);
		}
		size -= 2;
		Log.trace("%s - %s - %s - %s - %s - %s", ref.getWidth(), textWidth, ref.getHeight(), textHeight, text, size);
		ref.setFontSize(size);
	}

	private void createScreen() {
		screen = new Screen(clientMain, "tonegod/gui/style/atlasdef/style_map.gui.xml");
		screen.setUseTextureAtlas(true, "tonegod/gui/style/atlasdef/atlas.png");
		guiNode.addControl(screen);
		// screen.setUseKeyboardIcons(true);//do not use this on android, creates out of memory exception on sIII
		if(isAndroid()){
                    screen.setUseMultiTouch(true);
                }

	}

	public void changeState(GameState newState) {
		Log.debug("Changing state from '%s' to '%s'", this.getClass().getSimpleName(), newState.getClass().getSimpleName());
		stateManager.attach(newState);
		stateManager.detach(this);
	}

	@Override
	public void cleanup() {
		super.cleanup();
		Log.debug("Cleaning scene");

		for (int i = 0; i < guiNodeSpatials.size(); i++) {
			guiNode.detachChild(guiNodeSpatials.get(i));
		}

		for (int i = 0; i < screenElements.size(); i++) {
			screen.removeElement(screenElements.get(i));
		}

		for (int i = 0; i < guiNodeControls.size(); i++) {
			guiNode.removeControl(guiNodeControls.get(i));
		}

		clientMain.getConnection().removeConnectionListener(this);
	}

	/**
	 * Reference is only to be used to create Elements such as Buttons
	 *
	 * @return The screen element
	 */
	public Screen getScreen() {
		if (screen == null) {
			createScreen();
		}
		return screen;
	}

	public void addScreenElement(Element element) {
		screen.addElement(element);
		screenElements.add(element);
	}

	public void removeScreenElement(Element element) {
		screen.removeElement(element);
		screenElements.remove(element);
	}

	public void guiNodeAttachChild(Spatial spatial) {
		guiNode.attachChild(spatial);
		guiNodeSpatials.add(spatial);
	}

	public void guiNodeDetachChild(Spatial spatial) {
		guiNode.detachChild(spatial);
		guiNodeSpatials.remove(spatial);
	}

	public void guiNodeAddControl(Control control) {
		guiNode.addControl(control);
		guiNodeControls.add(control);
	}

	public void guiNodeRemoveControl(Control control) {
		guiNode.removeControl(control);
		guiNodeControls.remove(control);
	}

	@Override
	public void connectedToServer() {
	}

	@Override
	public void disconnectedFromServer() {
		// Test if we are in an online gameState
		if (!(this instanceof MainGameOnline || this instanceof OnlineGameMenu || this instanceof RoomGameState)) {
			return;
		}
		// show an error to the user
		Log.info("Connection to the server was lost.");
		clientMain.enqueue(new Callable<Spatial>() {
			@Override
			public Spatial call() throws Exception {
				AlertBox alert = new AlertBox(getScreen()) {
					@Override
					public void onButtonOkPressed(MouseButtonEvent evt, boolean toggled) {
						hide();
						removeScreenElement(this);
						changeState(new MainMenu(settings));
						cleanup();
					}
				};
				alert.showAsModal(true); // lock all other controls
				alert.setText("Connection lost!");
				alert.setMsg("The connection to server was lost.. Returning to MainMenu.");
				alert.setPosition(alert.getWidth() / 2, alert.getHeight() / 2); // center alert
				addScreenElement(alert);
				return alert;
			}
		});
	}

	@Override
	public void lobbyUpdate(List<Room> availableRooms) {
	}

	@Override
	public void playerJoin(Player player) {
	}

	@Override
	public void playerLeft(Player player) {
	}

	@Override
	public void roomCreated(Room room) {
	}

	@Override
	public void roomJoined(Room room) {
	}

	@Override
	public void roomUpdated(Room room) {
	}

	@Override
	public void gameStarted(StartGameBroadcast startGameBroadcast) {
	}

	@Override
	public void gameFinished(GameFinishedMessage gameFinishedMessage) {
	}

	@Override
	public void snapshotReceived(Snapshot snapshot) {
	}

	@Override
	public void loginResponse(LoginResponse loginResponse) {
	}
                public boolean isAndroid() {
            if(System.getProperty("java.vm.name").equalsIgnoreCase("Dalvik")){
                return true;
            } else {
                return false;
            }
        }
                

}
