/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client.gameStates;

import java.util.List;
import java.util.concurrent.Callable;

import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.lists.SelectList;
import tonegod.gui.controls.text.Label;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;

import curves.common.Log;
import curves.common.network.Objects.Player;
import curves.common.network.Room;

/**
 *
 * @author db-141205
 */
public class OnlineGameMenu extends GameState {

	private Label welcomeLabel;
	private SelectList gameSelectList;
	private Button newGameButton;
	private Button joinGameButton;
	private Button cancelButton;
	private Player player;

	OnlineGameMenu(AppSettings settings, Player player) {
		this.player = player;
		this.settings = settings;

	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);

		getScreen().parseLayout("Interface/Lobby.gui.xml", this);

		welcomeLabel = (Label) getScreen().getElementById("welcomeLabel");
		textSize(welcomeLabel);

		gameSelectList = (SelectList) getScreen().getElementById("gameSelectList");
		newGameButton = (Button) getScreen().getElementById("newGameButton");
		textSize(getScreen().getElementById("newGameButtonText"));
		joinGameButton = (Button) getScreen().getElementById("joinGameButton");
		textSize(getScreen().getElementById("joinGameButtonText"));
		cancelButton = (Button) getScreen().getElementById("cancelButton");
		textSize(getScreen().getElementById("cancelButtonText"));
		clientMain.getSound().setBackgoundMusicMenu();
		clientMain.getConnection().requestRooms();
	}

	public void gameSelectListChange() {
		joinGameButton.setIsEnabled(true);
	}

	public void newGameButtonClick(MouseButtonEvent evt, boolean toggled) {
		Log.info("Client wants to create a new room");
		createRoom();
	}

	public void joinGameButtonClick(MouseButtonEvent evt, boolean toggled) {
		Log.info("Client wants to join a room");
		joinRoom();
	}

	public void cancelButtonClick(MouseButtonEvent evt, boolean toggled) {
		cancel();
	}

	private void cancel() {
		Log.debug("cancel, falling back to Main Menu, logging of Player " + player.getUserName());
		changeState(new MainMenu(settings)); // back to main menu
		clientMain.getConnection().logout(player);
	}

	public void createRoom() {
		clientMain.getConnection().createRoom();
	}

	public void joinRoom() {
		clientMain.getConnection().joinRoom((Room) gameSelectList.getListItem(gameSelectList.getSelectedIndex()).getValue(), player);
	}

	@Override
	public void roomCreated(Room room) {
		changeState(new RoomGameState(room, settings)); // switch to created room
	}

	@Override
	public void roomJoined(Room room) {
		changeState(new RoomGameState(room, settings)); // join room
	}

	@Override
	public void lobbyUpdate(final List<Room> availableRooms) {
		if (gameSelectList == null) {
			return;
		}

		Log.info("A Lobby update was received");

		// Enqueue list update
		clientMain.enqueue(new Callable<Spatial>() {
			@Override
			public Spatial call() throws Exception {
				gameSelectList.removeAllListItems();

				for (int i = 0; i < availableRooms.size(); i++) {
					Room room = availableRooms.get(i);
					String adminName = room.getPlayers().get(0).getUserName();
					gameSelectList.addListItem("Room " + room.getId() + " (" + room.getPlayerCount() + "/" + room.getMaxPlayers() + ") - " + adminName, room);
				}
				return gameSelectList;
			}
		});
	}
}
