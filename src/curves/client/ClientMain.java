package curves.client;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;
import curves.client.gameStates.GameState;

import curves.client.gameStates.MainMenu;
import curves.client.network.Connection;
import curves.common.Log;
import curves.common.network.Objects.Player;
import curves.common.network.SerializeHelper;

/**
 * test
 *
 * @author david
 */
public class ClientMain extends SimpleApplication {

	public static final String backgoundMusicMenu = "backgoundMusicMenu";
	public static final String backgoundMusicGame = "backgoundMusicGame";
	public static final String backgoundMusicFinished = "backgoundMusicFinished";
	private String host = "87.106.49.177";
	//private String host = "127.0.0.1";
	//private String host = "192.168.0.100";
	public static final int CMDUPDATERATE = 20; // input updates/second
	private int port = 1337;
	private Connection connection;
	private MainMenu menuGameState;
	public static AssetManager assetManager;
	private Sound sound;
	private Node rootNode;

	public static void main(String[] args) {
		// DO NOT USE LOGGER IN MAIN



		AppSettings settings = new AppSettings(true);

		settings.setRenderer(AppSettings.LWJGL_OPENGL2);

		settings.setFrameRate(60);
		System.out.println("Starting client");
		ClientMain app = new ClientMain();
		app.setPauseOnLostFocus(false);
		app.setSettings(settings);
		app.start();

	}

	@Override
	public void simpleInitApp() {
		assetManager = getAssetManager();

		this.rootNode = rootNode;
		// Disable debug stats
		// setDisplayFps(false); // disable fps counter
		//setDisplayStatView(false);// disable stats
		this.sound = new Sound(assetManager);
		SerializeHelper.register();
		connection = new Connection(host, port);
		menuGameState = new MainMenu(settings);
		stateManager.attach(menuGameState);
		connection.connectToServer();

		getFlyByCamera().setEnabled(false);
	}

	@Override
	public void simpleUpdate(float tpf) {
	}

	@Override
	public void destroy() {
		connection.close();
		super.destroy();
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnectionId(String connectionId) {
		settings.putString("connectionId", connectionId);
	}

	public String getConnectionId() {
		return settings.getString("connectionId");
	}

	public void setMainPlayer(Player player) {
		settings.put("mainPlayer", player);
	}

	public Player getMainPlayer() {
		return (Player) settings.get("mainPlayer");
	}

	public void offlineConnectionStuff() {
		connection = new Connection("127.0.0.1", 1337);
		connection.connectToServer();
	}

	public Sound getSound() {
		return this.sound;
	}
}
