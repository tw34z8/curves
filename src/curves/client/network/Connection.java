package curves.client.network;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import com.jme3.network.Message;
import com.jme3.network.Network;

import curves.common.Log;
import curves.common.network.Objects.Player;
import curves.common.network.Room;
import curves.common.network.messages.LobbyMessage;
import curves.common.network.messages.LobbyRequestMessage;
import curves.common.network.messages.LoginRequest;
import curves.common.network.messages.LoginResponse;
import curves.common.network.messages.LogoutRequest;
import curves.common.network.messages.game.*;
import curves.common.network.messages.room.*;

public class Connection implements ClientStateListener {

	private Client clientInstance;
	ServerListener serverListener = new ServerListener(this);
	List<ConnectionListener> connectionListeners = new CopyOnWriteArrayList<ConnectionListener>();
	String host;
	int port;

	public Connection(String host, int port) {
		this.host = host;
		this.port = port;
	}

	// TODO maybe start thread and try connect in a loop
	public boolean connectToServer() {
		try {
			Log.info("Trying to connect to: '%s':'%s'", host, port);
			clientInstance = Network.connectToServer(host, port);
			clientInstance.addMessageListener(serverListener);
			clientInstance.addClientStateListener(this);
			clientInstance.start();
			Log.info("Successfully connected to: '%s':'%s'", host, port);
		} catch (IOException ex) {
			Log.info("Error while connecting: '%s'", ex);
			ex.printStackTrace();
		}
		return isConnected();
	}

	public boolean isConnected() {
		return clientInstance != null && clientInstance.isConnected();
	}

	private void sendMessage(Message message) {
		//Log.info("Sending message: '%s'", message.getClass().getSimpleName());
		clientInstance.send(message);
	}

	@Override
	public void clientConnected(Client c) {
		Log.info("Connected to server.");
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.connectedToServer();
		}
	}

	@Override
	public void clientDisconnected(Client c, DisconnectInfo info) {
		Log.info("Disconnected from server.");
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.disconnectedFromServer();
		}
	}

	public void addConnectionListener(ConnectionListener connectionListener) {
		connectionListeners.add(connectionListener);
		// Tell new listener if connected or not
		if (isConnected()) {
			connectionListener.connectedToServer();
		} else {
			connectionListener.disconnectedFromServer();
		}
	}

	public void removeConnectionListener(ConnectionListener connectionListener) {
		connectionListeners.remove(connectionListener);
	}

	/**
	 * Called from ServerListener, if a lobby update occured
	 *
	 * @param lobbyMessage
	 */
	public void lobbyUpdate(LobbyMessage lobbyMessage) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.lobbyUpdate(lobbyMessage.getRooms());
		}
	}

	/**
	 * Called from ServerListener, if a new room was created
	 *
	 * @param roomCreated
	 */
	public void roomCreated(RoomCreated roomCreated) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.roomCreated(roomCreated.getRoom());
		}
	}

	/**
	 * Called from ServerListener, if a room was joined
	 *
	 * @param roomJoined
	 */
	public void roomJoined(RoomJoined roomJoined) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.roomJoined(roomJoined.getRoom());
		}
	}

	/**
	 * Called from ServerListener, if a room was changed
	 *
	 * @param roomUpdate
	 */
	public void roomUpdate(RoomUpdate roomUpdate) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.roomUpdated(roomUpdate.getRoom());
		}
	}

	public void startGame(StartGameBroadcast startGameBroadcast) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.gameStarted(startGameBroadcast);
		}

	}

	void snapshotReceived(Snapshot snapshot) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.snapshotReceived(snapshot);
		}
	}

	void loginResponse(LoginResponse loginResponse) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.loginResponse(loginResponse);
		}
	}

	public void gameFinished(GameFinishedMessage gameFinishedMessage) {
		for (ConnectionListener connectionListener : connectionListeners) {
			connectionListener.gameFinished(gameFinishedMessage);
		}
	}

	public void close() {
		if (isConnected()) {
			clientInstance.close();
		}
	}

	/* Requests part */
	public void requestRooms() {
		sendMessage(new LobbyRequestMessage());
	}

	public void createRoom() {
		sendMessage(new CreateRoom());
	}

	public void joinRoom(Room room, Player player) {
		sendMessage(new JoinRoom(room, player));
	}

	public void leaveRoom(Room room) {
		sendMessage(new LeaveRoom(room.getId()));
	}

	public void login(String userName) {
		sendMessage(new LoginRequest(userName));
	}

	public void startGame(StartGame startGame) {
		sendMessage(startGame);
	}

	public void directionUpdate(DirectionUpdate directionUpdate) {
		sendMessage(directionUpdate);
	}

	public void logout(Player player) {
		sendMessage(new LogoutRequest(player));
	}
}
