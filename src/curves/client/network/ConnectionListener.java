package curves.client.network;

import java.util.List;

import curves.common.network.Objects.Player;
import curves.common.network.Room;
import curves.common.network.messages.LoginResponse;
import curves.common.network.messages.game.GameFinishedMessage;
import curves.common.network.messages.game.Snapshot;
import curves.common.network.messages.game.StartGameBroadcast;

/**
 * Interface to implement to listen to Connection events
 *
 * @author Death
 *
 */
public interface ConnectionListener {

	/* Basic methods */
	public void connectedToServer();

	public void disconnectedFromServer();

	/* Room stuff */
	public void lobbyUpdate(final List<Room> availableRooms);

	public void playerJoin(final Player player);

	public void playerLeft(final Player player);

	public void roomCreated(final Room room);

	public void roomJoined(final Room room);

	public void roomUpdated(final Room room);

	/* TODO Game stuff */
	public void gameStarted(StartGameBroadcast startGameBroadcast);

	public void gameFinished(GameFinishedMessage gameFinishedMessage);

	public void snapshotReceived(Snapshot snapshot);

	public void loginResponse(LoginResponse loginResponse);

}
