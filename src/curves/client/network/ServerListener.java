package curves.client.network;

import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;

import curves.common.Log;
import curves.common.network.messages.LobbyMessage;
import curves.common.network.messages.LoginResponse;
import curves.common.network.messages.game.GameFinishedMessage;
import curves.common.network.messages.game.Snapshot;
import curves.common.network.messages.game.StartGameBroadcast;
import curves.common.network.messages.room.RoomCreated;
import curves.common.network.messages.room.RoomJoined;
import curves.common.network.messages.room.RoomUpdate;

public class ServerListener implements MessageListener<Client> {

	Connection connection;

	public ServerListener(Connection connection) {
		this.connection = connection;
	}

	@Override
	public void messageReceived(Client source, Message message) {
		//Log.info("Message received: '%s'", message.getClass().getSimpleName());
		if (message instanceof LobbyMessage) {
			LobbyMessage roomMessage = (LobbyMessage) message;
			connection.lobbyUpdate(roomMessage);
		} else if (message instanceof RoomCreated) {
			RoomCreated roomCreated = (RoomCreated) message;
			connection.roomCreated(roomCreated);
		} else if (message instanceof RoomJoined) {
			RoomJoined roomJoined = (RoomJoined) message;
			connection.roomJoined(roomJoined);
		} else if (message instanceof RoomUpdate) {
			RoomUpdate roomUpdate = (RoomUpdate) message;
			connection.roomUpdate(roomUpdate);
		} else if (message instanceof StartGameBroadcast) {
			StartGameBroadcast startGameBroadcast = (StartGameBroadcast) message;
			connection.startGame(startGameBroadcast);
		} else if (message instanceof Snapshot) {
			Snapshot snapshot = (Snapshot) message;
			connection.snapshotReceived(snapshot);
		} else if (message instanceof LoginResponse) {

			LoginResponse loginResponse = (LoginResponse) message;
			connection.loginResponse(loginResponse);
		} else if (message instanceof GameFinishedMessage) {
			connection.gameFinished((GameFinishedMessage) message);
		}
	}
}
