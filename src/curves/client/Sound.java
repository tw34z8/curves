/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import static curves.client.ClientMain.assetManager;
import curves.common.Log;
import java.util.Random;

/**
 *
 * @author io
 */
public class Sound {

	private AssetManager assetManager;
	private AudioNode[] explosions;
	private AudioNode backgroundMusic;
	private AudioNode[] shots;

	public Sound(AssetManager assetManager) {
		this.assetManager = assetManager;
		shots = new AudioNode[4];
		explosions = new AudioNode[8];


		loadSounds();
	}

	private void loadSounds() {
		for (int i = 0; i < explosions.length; i++) {
			explosions[i] = new AudioNode(assetManager, "Sounds/explosion-0" + (i + 1) + ".wav");
			explosions[i].setPositional(false);
			explosions[i].setReverbEnabled(false);
			explosions[i].setLooping(false);
		}
		for (int i = 0; i < shots.length; i++) {
			shots[i] = new AudioNode(assetManager, "Sounds/shoot-0" + (i + 1) + ".wav");
			shots[i].setPositional(false);
			shots[i].setReverbEnabled(false);
			shots[i].setLooping(false);
		}
	}

	public void shoot() {
		shots[new Random().nextInt(shots.length)].playInstance();
	}

	public void explosion() {
		explosions[new Random().nextInt(explosions.length)].playInstance();
	}

	public void setBackgoundMusicMenu() {
		stop();
		backgroundMusic = new AudioNode(assetManager, "Sounds/menuloop.wav", false);
		backgroundMusic.setPositional(false);
		backgroundMusic.setLooping(true);  // activate continuous playing
		backgroundMusic.play(); // play continuously!
	}

	public void setBackgoundMusicGame() {
		stop();
		backgroundMusic = new AudioNode(assetManager, "Sounds/gameloop.wav", false);
		backgroundMusic.setPositional(false);
		backgroundMusic.setLooping(true);  // activate continuous playing
		backgroundMusic.play(); // play continuously!
	}

	public void setBackgoundMusicFinished() {
		stop();
		backgroundMusic = new AudioNode(assetManager, "Sounds/finishedloop.wav", false);
		backgroundMusic.setPositional(false);
		backgroundMusic.setLooping(true);  // activate continuous playing
		backgroundMusic.play(); // play continuously!
	}

	private void stop() {
		try {
			backgroundMusic.stop();
			Log.debug("stopped");
		} catch (NullPointerException exc) {
		}
	}
}
