/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client;

import com.jme3.effect.ParticleEmitter;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import curves.common.Log;
import org.apache.commons.pool.impl.GenericObjectPool;

/**
 *
 * @author io
 */
public class ExplosionPool extends GenericObjectPool<ParticleEmitter> {

	public ExplosionPool(ExplosionFactory explosionFactory) {
		super(explosionFactory);
		this.setMaxActive(1000);
		this.setMaxIdle(1000);
		this.setMinIdle(20);
	}

	@Override
	public ParticleEmitter borrowObject() throws Exception {
		ParticleEmitter particleEmitter = super.borrowObject();
		particleEmitter.addControl(new ExplosionControler(0.8f, this));
		return particleEmitter;
	}

	@Override
	public void returnObject(ParticleEmitter obj) throws Exception {
		obj.removeControl(ExplosionControler.class);
		super.returnObject(obj);
	}
}
