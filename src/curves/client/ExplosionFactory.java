/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client;

import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.BatchNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import curves.common.Log;
import org.apache.commons.pool.PoolableObjectFactory;

/**
 *
 * @author io
 */
public class ExplosionFactory implements PoolableObjectFactory<ParticleEmitter> {

	private AssetManager assetManager;
	private Node rootNode;

	public ExplosionFactory(AssetManager assetManager, Node rootNode) {
		super();
		this.assetManager = assetManager;
		this.rootNode = rootNode;
	}

	@Override
	public ParticleEmitter makeObject() throws Exception {
		BatchNode node = new BatchNode();
		ParticleEmitter debris =
			new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 20);
		Material debris_mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
		debris_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.AlphaAdditive);
		debris_mat.setTexture("Texture", assetManager.loadTexture("Textures/Glow3.png"));
		debris.setMaterial(debris_mat);
		//debris.setImagesX(3);
		//debris.setImagesY(3); // 3x3 texture animation
		//debris.setRotateSpeed(4);
		debris.setStartSize(0.1f);
		debris.setEndSize(0.0f);
		debris.setLowLife(0.1f);
		debris.setHighLife(0.8f);
		debris.setFacingVelocity(true);
		debris.setSelectRandomImage(true);
		debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0, 1));
		debris.setStartColor(ColorRGBA.White);
		debris.setEndColor(ColorRGBA.White);
		debris.setParticlesPerSec(0f);
		debris.setGravity(0, 0, 0);
		//debris.emitAllParticles();
		debris.getParticleInfluencer().setVelocityVariation(1f);
		node.attachChild(debris);
		node.batch();
		debris.setBatchHint(Spatial.BatchHint.Always);
		//debris.emitAllParticles();
		//debris.addControl(new ExplosionControler(0.5f));
		Log.debug("new explosion");
		rootNode.attachChild(debris);
		return debris;

	}

	@Override
	public void destroyObject(ParticleEmitter obj) throws Exception {
		obj.removeFromParent();
	}

	@Override
	public boolean validateObject(ParticleEmitter obj) {
		return false;
	}

	@Override
	public void activateObject(ParticleEmitter obj) throws Exception {
	}

	@Override
	public void passivateObject(ParticleEmitter obj) throws Exception {
	}
}
