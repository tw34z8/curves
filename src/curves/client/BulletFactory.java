/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client;

import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;
import static curves.client.ClientMain.assetManager;
import curves.common.Log;
import org.apache.commons.pool.PoolableObjectFactory;

/**
 *
 * @author io
 */
public class BulletFactory implements PoolableObjectFactory<Node> {

	Node guiNode;

	public BulletFactory(Node guiNode) {
		this.guiNode = guiNode;
	}

	@Override
	public Node makeObject() throws Exception {
		Log.debug("new bullet");
		return getSpatial("Bullet");
	}

	@Override
	public void destroyObject(Node obj) throws Exception {
		obj.removeFromParent();
	}

	@Override
	public boolean validateObject(Node obj) {
		return false;
	}

	@Override
	public void activateObject(Node obj) throws Exception {
	}

	@Override
	public void passivateObject(Node obj) throws Exception {
	}

	private Node getSpatial(String name) {
		Node node = new Node(name);
//        load picture
		Picture pic = new Picture(name);
		Texture2D tex = (Texture2D) assetManager.loadTexture("Textures/" + name + ".png");
		pic.setTexture(assetManager, tex, true);

//        adjust picture
		float width = tex.getImage().getWidth();
		float height = tex.getImage().getHeight();
		pic.setWidth(width);
		pic.setHeight(height);
		pic.move(-width / 2f, -height / 2f, 0);

//        add a material to the picture
		Material picMat = new Material(assetManager, "Common/MatDefs/Gui/Gui.j3md");
		picMat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.AlphaAdditive);
		node.setMaterial(new Material());

//        set the radius of the spatial
//        (using width only as a simple approximation)
		node.setUserData("radius", width / 2);

//        attach the picture to the node and return it
		node.attachChild(pic);
		guiNode.attachChild(node);
		return node;
	}
}
