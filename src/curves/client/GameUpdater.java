package curves.client;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.BatchNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;
import com.jme3.util.BufferUtils;
import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

import curves.common.Log;
import curves.common.Movement;
import curves.common.network.Objects.Bullet;
import curves.common.network.Objects.Explosion;
import curves.common.network.Objects.GameObject;
import curves.common.network.Objects.Player;
import curves.common.network.messages.game.DirectionUpdate;
import curves.common.network.messages.game.Snapshot;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;

public class GameUpdater {

	private String gameID;
	private Map<Player, Integer> directionUpdateCount;
	private double localTime; // for interpolation stuff
	Node offNode;
	AppSettings settings;
	private Snapshot[] snapshotBuffer;
	private final List<Player> players;
	private CopyOnWriteArrayList<Spatial> bullets = new CopyOnWriteArrayList<>();
	private final Map<Player, Geometry> geometries = new HashMap<>();
	private final Map<Player, Geometry> predictionGeometries = new HashMap<>();
	private Map<Player, List<DirectionUpdate>> unacknowledegedDirectionUpdates;
	private final AssetManager assetManager;
	private final ClientMain clientMain;
	private float tpfCounter; //for CMDUPDATERADE
	private float animationtpfConter;
	private Node bulletNode;
	private Node guiNode;
	private BulletFactory bulletFactory;
	private BulletPool bulletPool;
	private ExplosionFactory explosionFactory;
	private ExplosionPool explosionPool;
	private Node rootNode;
	private Camera camera;
	private Player explosion;

	public GameUpdater(List<Player> players, AssetManager assetManager, Node offNode, AppSettings settings, Snapshot[] snapshotBuffer,
		Map<Player, List<DirectionUpdate>> unacknowledegedDirectionUpdates, ClientMain clientMain, String gameID) {
		directionUpdateCount = new HashMap<Player, Integer>();
		this.gameID = gameID;
		this.settings = settings;
		this.assetManager = assetManager;
		this.snapshotBuffer = snapshotBuffer;
		this.offNode = offNode;
		this.players = players;
		this.unacknowledegedDirectionUpdates = unacknowledegedDirectionUpdates;
		this.clientMain = clientMain;


		this.rootNode = clientMain.getRootNode();
		this.guiNode = clientMain.getGuiNode();
		this.camera = clientMain.getCamera();
		this.bulletFactory = new BulletFactory(guiNode);
		this.bulletPool = new BulletPool(bulletFactory);
		this.explosionFactory = new ExplosionFactory(assetManager, rootNode);
		this.explosionPool = new ExplosionPool(explosionFactory);

		for (Player player : players) {
			geometries.put(player, createPlayerGeometry(player.getColor()));
			predictionGeometries.put(player, createPlayerGeometry(player.getColor().mult(1.f)));
			directionUpdateCount.put(player, 0);
		}
		this.explosion = new Player();
		explosion.setColor(ColorRGBA.Black);
		geometries.put(explosion, createPlayerGeometry(explosion.getColor()));
		//predictionGeometries.put(player, createPlayerGeometry(player.getColor().mult(1.f)));

		//        setup the bulletNode

		bulletNode = new Node("bullets");
		//guiNode.attachChild(bulletNode);

	}

	private Geometry createPlayerGeometry(ColorRGBA color) {
		Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		material.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off); // turn culling off
		material.setColor("Color", color);
		material.preload(clientMain.getRenderManager());
		Mesh mesh = new Mesh();
		Geometry geometry = new Geometry(color.toString(), mesh);
		geometry.setMaterial(material);
		//offNode.attachChild(geometry);
		return geometry;
	}

	/**
	 * Updates the world for all players
	 *
	 * TODO: this method needs definetly some refactoring
	 *
	 * @param tpf
	 */
	public void updateWorld(float tpf) {
		Geometry playerGeometry = geometries.get(explosion);
		playerGeometry.setLocalTranslation(-10000, -10000, -10000);
		tpfCounter += tpf;
		Vector3f multVector2 = new Vector3f(settings.getWidth() / 1920f, settings.getHeight() / 1080f, 0);

		Vector3f multVector = new Vector3f(settings.getWidth() / -1920f, settings.getHeight() / 1080f, 0);
		Vector3f subtractVector = new Vector3f(1920f / 2f, 1080f / 2f, 0);

		Vector3f from;
		Vector3f to;

		for (Spatial spatial : bullets) {
			try {
				//spatial.removeFromParent();
				bulletPool.returnObject((Node) (spatial));
				bullets.remove(spatial);

			} catch (Exception ex) {
				Log.debug("ERROR on returning Bullet to POOL");
			}
		}
		//bulletNode.detachAllChildren();
		if (snapshotBuffer[0] != null && snapshotBuffer[1] != null) {
			Iterator<GameObject> iter = snapshotBuffer[1].getGameObjects().iterator();

			while (iter.hasNext()) {
				GameObject gameObject = iter.next();
				if (gameObject instanceof Explosion) {

					Log.debug("bam");
					clientMain.getSound().explosion();
					float z = camera.getScreenCoordinates(new Vector3f(0, 0, 0f)).getZ();
					Vector3f emitterPosition = camera.getWorldCoordinates(new Vector2f(gameObject.getPosition().clone().mult(multVector2).x, gameObject.getPosition().clone().mult(multVector2).y), z);
					Vector3f removePosition = gameObject.getPosition().clone().subtract(subtractVector).mult(multVector);
					try {
						ParticleEmitter particleEmitter = explosionPool.borrowObject();
						particleEmitter.setLocalTranslation(emitterPosition);
						particleEmitter.emitAllParticles();

						plotExplosion(removePosition, removePosition, playerGeometry, explosion);

					} catch (Exception ex) {
						ex.printStackTrace();
					}
					iter.remove();


				}
			}

			for (GameObject gameObject1 : snapshotBuffer[1].getGameObjects()) {
				boolean firstOccurence = true;
				if (gameObject1 instanceof Bullet) {

					for (GameObject gameObject : snapshotBuffer[0].getGameObjects()) {
						if ((gameObject.getID().equals(gameObject1.getID()))) {
							firstOccurence = false;

							double timeDiff = snapshotBuffer[1].getTime() - snapshotBuffer[0].getTime();
							double relativeTimeDiff = localTime - snapshotBuffer[0].getTime();
							double amnt = relativeTimeDiff / timeDiff;

							from = gameObject.getPosition().clone().mult(multVector2);//.subtract(subtractVector).mult(multVector);
							to = gameObject1.getPosition().clone().mult(multVector2);//.subtract(subtractVector).mult(multVector);

							//Log.debug("FROM: " + from + "TO: " + to);

							to = new Vector3f().interpolate(from, to, (float) amnt); // interpolate
							//Log.debug("INTERPOLATED: " + to);
							try {
								Spatial bullet = bulletPool.borrowObject();
								bullet.setLocalTranslation(to);
								//        rotation
								float actualRotation = getAngleFromVector(gameObject1.getDirectionVector());
								bullet.setUserData("rotation", actualRotation);
								bullet.rotate(0, 0, actualRotation);

								bullets.add(bullet);

								//Log.debug(player.getPosition());
								//Log.debug(player.getPosition());

								// bullet.addControl(new BulletControl(player.getDirectionVector(), settings.getWidth(), settings.getHeight()));
								//bulletNode.attachChild(bullet);
							} catch (Exception ex) {
								Log.debug(ex);
							}
							continue;

						}

					}
				}
				if (firstOccurence) {
					if (gameObject1 instanceof Bullet) {
						clientMain.getSound().shoot();
					}
				}

			}
		}
		//Player interpolation, prediction and input forwarding to server
		for (Player player : players) {
			// Apply snapshots from server
			//2 or more user commands are transmitted within the same packet.
			//@see https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking

			int direction;
			if (player.isKey_left() && player.isKey_right()) {
				direction = 2;
			} else {
				if (player.isKey_left()) {
					direction = -1;
				} else if (player.isKey_right()) {
					direction = 1;
				} else {
					direction = 0;
				}
			}
			if ((!player.isDead() && player.getConnectionID().equals(clientMain.getConnectionId())) && (tpfCounter >= ((1000.0 / ClientMain.CMDUPDATERATE) / 1000.0) || direction != player.getDirection())) { //player darf nicht tot sein, das letzte update ist CMDUPDATERATE her oder es hat sich was geändert, dient dem bundeln von updates
				sendDirectionUpdate(player, direction, tpfCounter);

			}
			if (player.getConnectionID().equals(clientMain.getConnectionId())) {
				from = player.getPosition().clone().subtract(subtractVector).mult(multVector);
				Movement.applyDirectionUpdate(player, new DirectionUpdate(player.getID(), gameID, direction, 0, tpf));
				to = player.getPosition().clone().subtract(subtractVector).mult(multVector);
				playerGeometry = predictionGeometries.get(player);
				plotLine(from, to, playerGeometry, player);
			}


			if (snapshotBuffer[0] == null || snapshotBuffer[1] == null) {
				continue;
			}
			playerGeometry = predictionGeometries.get(player);
			Player[] playerSnapshot = getPlayerPositions(player);
			from = playerSnapshot[0].getPosition().clone().subtract(subtractVector).mult(multVector);
			to = playerSnapshot[1].getPosition().clone().subtract(subtractVector).mult(multVector);

			double timeDiff = snapshotBuffer[1].getTime() - snapshotBuffer[0].getTime();
			double relativeTimeDiff = localTime - snapshotBuffer[0].getTime();
			double amnt = relativeTimeDiff / timeDiff;
			to = new Vector3f().interpolate(from, to, (float) amnt); // interpolate

			if (amnt <= 1.0) { //anti exrapolation prevention system
				plotLine(from, to, playerGeometry, playerSnapshot[0]);
			}

		}
		localTime += tpf * 1000000000d;
		if (tpfCounter >= ((1000.0 / ClientMain.CMDUPDATERATE) / 1000.0)) {
			tpfCounter = 0;
		}
		animationtpfConter += tpf;
		if (animationtpfConter > 0.6) {
			animationtpfConter = 0;
		}


	}

	/**
	 * Plots a line with the given Vertices using the given geometry
	 *
	 * @param lineVerticies
	 * @param geometry
	 */
	private void plotExplosion(Vector3f from, Vector3f to, Geometry geometry, Player player) {
		final Mesh mesh = geometry.getMesh();

		float width = 45.0f * settings.getWidth() / 1920.0f;

		Vector3f perp0 = from.clone().subtract(to); // get direction vector
		Vector3f perp1 = new Vector3f(-perp0.y, perp0.x, 0).normalize(); // get vector perpendicular to direction vector

		Vector3f[] vertices = new Vector3f[18];
		mesh.setMode(Mesh.Mode.TriangleStrip);

		float angle = 0;
		float rate = FastMath.TWO_PI / 15;
		int cmp = 18;
		for (int i = 0; i < cmp; i++) {//+3 wenns ein voller kreis sein soll
			if (i % 3 == 0) {
				vertices[i] = new Vector3f(0.0f, 0.0f, 0.0f);
			} else {
				vertices[i] = new Vector3f(FastMath.cos(angle), FastMath.sin(angle), 0.0f).mult(width);
			}

			angle += rate;
		}

		// get vertex positions (triangeStrip) and offset them by subtracting/adding the perpendicular vector (multiplied with width)
		/*vertices[0] = from.clone().subtract(perp1.clone().mult(width));
		 vertices[1] = from.clone().add(perp1.clone().mult(width));
		 vertices[2] = to.clone().subtract(perp1.clone().mult(width));

		 vertices[3] = to.clone().subtract(perp1.clone().mult(width));
		 vertices[4] = to.clone().add(perp1.clone().mult(width));
		 vertices[5] = from.clone().subtract(perp1.clone().mult(width));*/

		FloatBuffer createFloatBuffer = BufferUtils.createFloatBuffer(vertices);
		mesh.setBuffer(VertexBuffer.Type.Position, 3, createFloatBuffer);


		mesh.updateBound();
		geometry.setLocalTranslation(to);
		Matrix3f rotationMatrix = new Matrix3f((float) Math.cos(angle), -1 * (float) Math.sin(angle), 0, (float) Math.sin(angle), (float) Math.cos(angle), 0, 0, 0, 1);
		geometry.setLocalRotation(rotationMatrix);
		geometry.updateModelBound();


		offNode.attachChild(geometry);

		//offNode.updateGeometricState();

	}

	/**
	 * Plots a line with the given Vertices using the given geometry
	 *
	 * @param lineVerticies
	 * @param geometry
	 */
	private void plotLine(Vector3f from, Vector3f to, Geometry geometry, Player player) {
		final Mesh mesh = geometry.getMesh();

		float width = 5.0f * settings.getWidth() / 1920.0f;

		Vector3f perp0 = from.clone().subtract(to); // get direction vector
		Vector3f perp1 = new Vector3f(-perp0.y, perp0.x, 0).normalize(); // get vector perpendicular to direction vector

		Vector3f[] vertices = new Vector3f[18];
		mesh.setMode(Mesh.Mode.TriangleStrip);

		float angle = 0;
		float rate = FastMath.TWO_PI / 15;
		int cmp = 15;
		if (animationtpfConter <= 0.3) {
			cmp += 3;
		}

		for (int i = 0; i < cmp; i++) {//+3 wenns ein voller kreis sein soll
			if (i % 3 == 0) {
				vertices[i] = new Vector3f(0.0f, 0.0f, 0.0f);
			} else {
				vertices[i] = new Vector3f(FastMath.cos(angle), FastMath.sin(angle), 0.0f).mult(width);
			}

			angle += rate;
		}

		// get vertex positions (triangeStrip) and offset them by subtracting/adding the perpendicular vector (multiplied with width)
		/*vertices[0] = from.clone().subtract(perp1.clone().mult(width));
		 vertices[1] = from.clone().add(perp1.clone().mult(width));
		 vertices[2] = to.clone().subtract(perp1.clone().mult(width));

		 vertices[3] = to.clone().subtract(perp1.clone().mult(width));
		 vertices[4] = to.clone().add(perp1.clone().mult(width));
		 vertices[5] = from.clone().subtract(perp1.clone().mult(width));*/

		FloatBuffer createFloatBuffer = BufferUtils.createFloatBuffer(vertices);
		mesh.setBuffer(VertexBuffer.Type.Position, 3, createFloatBuffer);


		mesh.updateBound();
		geometry.setLocalTranslation(to);
		if (player.getDirectionVector().x > 0.0) {
			angle = FastMath.PI - FastMath.asin(player.getDirectionVector().clone().normalize().y);
		} else {
			angle = FastMath.asin(player.getDirectionVector().clone().normalize().y);
		}
		Matrix3f rotationMatrix = new Matrix3f((float) Math.cos(angle), -1 * (float) Math.sin(angle), 0, (float) Math.sin(angle), (float) Math.cos(angle), 0, 0, 0, 1);
		geometry.setLocalRotation(rotationMatrix);
		geometry.updateModelBound();


		offNode.attachChild(geometry);

		//offNode.updateGeometricState();

	}

	/**
	 * Returns the playerPosition from two Snapshots
	 *
	 * @param player
	 * @return
	 */
	public Player[] getPlayerPositions(Player player) {
		Player[] player1 = new Player[2];

		final String playerId = player.getID();
		for (GameObject p1 : snapshotBuffer[0].getGameObjects()) {
			if (p1.getID().equals(playerId)) {
				player1[0] = (Player) p1;
			}
		}
		for (GameObject p1 : snapshotBuffer[1].getGameObjects()) {
			if (p1.getID().equals(playerId)) {
				player1[1] = (Player) p1;
			}
		}

		if (player1[0] == null || player1[1] == null) {
			final String string = "Could not determine playerPosition!";
			Log.error(string);
			throw new RuntimeException(string);
		}

		return player1;
	}

	/**
	 * @return the localTime
	 */
	public double getLocalTime() {
		return localTime;
	}

	/**
	 * @param localTime the localTime to set
	 */
	public void setLocalTime(double localTime) {
		this.localTime = localTime;
	}

	/**
	 * Sends a direction update to the server and holds it in its
	 * unacklowedgedDirectionUpdate list
	 *
	 * @param player Player who sends an update
	 * @param direction The direction he wants to go
	 */
	private void sendDirectionUpdate(Player player, int direction, float tpf) {
		final List<DirectionUpdate> list = unacknowledegedDirectionUpdates.get(player);
		final int directionUpdateId = directionUpdateCount.get(player) + 1;
		final DirectionUpdate directionUpdate = new DirectionUpdate(player.getID(), gameID, direction, directionUpdateId, tpf);
		list.add(directionUpdate);
		directionUpdateCount.put(player, directionUpdateId);
		clientMain.getConnection().directionUpdate(directionUpdate);

	}

	private Spatial getSpatial(String name) {
		Node node = new Node(name);
//        load picture
		Picture pic = new Picture(name);
		Texture2D tex = (Texture2D) assetManager.loadTexture("Textures/" + name + ".png");
		pic.setTexture(assetManager, tex, true);

//        adjust picture
		float width = tex.getImage().getWidth();
		float height = tex.getImage().getHeight();
		pic.setWidth(width);
		pic.setHeight(height);
		pic.move(-width / 2f, -height / 2f, 0);

//        add a material to the picture
		Material picMat = new Material(assetManager, "Common/MatDefs/Gui/Gui.j3md");
		picMat.getAdditionalRenderState().setBlendMode(BlendMode.AlphaAdditive);
		node.setMaterial(new Material());

//        set the radius of the spatial
//        (using width only as a simple approximation)
		node.setUserData("radius", width / 2);

//        attach the picture to the node and return it
		node.attachChild(pic);
		return node;
	}

	private static float getAngleFromVector(Vector3f vec) {
		Vector2f vec2 = new Vector2f(vec.x, vec.y);
		return vec2.getAngle();
	}
}
