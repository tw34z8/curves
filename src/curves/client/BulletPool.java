/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import curves.common.Log;
import org.apache.commons.pool.impl.GenericObjectPool;

/**
 *
 * @author io
 */
public class BulletPool extends GenericObjectPool<Node> {

	public BulletPool(BulletFactory bulletFactory) {
		super(bulletFactory);
		this.setMaxActive(1000);
		this.setMaxIdle(1000);
		this.setMinIdle(20);



	}

	@Override
	public Node borrowObject() throws Exception {
		return super.borrowObject();
	}

	@Override
	public void returnObject(Node obj) throws Exception {
		obj.rotate(0, 0, -1 * (float) obj.getUserData("rotation")); //rotation rückgängig machen
		obj.setLocalTranslation(new Vector3f(-1000, -1000, 0));
		super.returnObject(obj);
	}
}
