/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.client;

import com.jme3.effect.ParticleEmitter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import curves.common.Log;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author io
 */
public class ExplosionControler extends AbstractControl {

	private float tpfCounter;
	private float lifeTime;
	private ExplosionPool explosionPool;

	public ExplosionControler(float lifeTime, ExplosionPool explosionPool) {
		this.lifeTime = lifeTime;
		this.explosionPool = explosionPool;
	}

	@Override
	protected void controlUpdate(float tpf) {
		if (tpfCounter > lifeTime) {
			try {
				explosionPool.returnObject((ParticleEmitter) this.spatial);

			} catch (Exception ex) {
				Log.debug(ex);
			}
		}
		tpfCounter += tpf;
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {
	}
}
