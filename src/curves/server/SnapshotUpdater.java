/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.server;

import curves.common.network.Game;

/**
 *
 * @author io
 */
public class SnapshotUpdater extends Thread {
	private Game game;
	private ServerMain serverMain;

	public SnapshotUpdater(Game game, ServerMain serverMain) {
		this.game = game;
		this.serverMain = serverMain;
		start();
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			try {
				serverMain.sendSnapshot(game);
				Thread.sleep(1000 / ServerMain.UPDATERATE);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}
}
