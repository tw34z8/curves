package curves.server;

import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;

import curves.common.network.messages.LobbyMessage;
import curves.common.network.messages.LobbyRequestMessage;
import curves.common.network.messages.LoginRequest;
import curves.common.network.messages.LogoutRequest;
import curves.common.network.messages.game.DirectionUpdate;
import curves.common.network.messages.game.StartGame;
import curves.common.network.messages.room.CreateRoom;
import curves.common.network.messages.room.JoinRoom;
import curves.common.network.messages.room.LeaveRoom;

public class ClientListener implements MessageListener<HostedConnection> {

	private ServerMain serverMain;

	public ClientListener(ServerMain serverMain) {
		this.serverMain = serverMain;
	}

	@Override
	public void messageReceived(HostedConnection source, Message message) {
		// String userName = source.getAttribute(ServerMain.USER_NAME_ATTRIBUTE);
		// Log.info("Message received from: '%s' : '%s'", userName, message.getClass().getSimpleName());
		if (message instanceof LobbyRequestMessage) {
			source.send(new LobbyMessage(serverMain.getRooms()));
		} else if (message instanceof CreateRoom) {
			serverMain.createRoom(source);
		} else if (message instanceof JoinRoom) {
			serverMain.joinRoom(source, (JoinRoom) message);
		} else if (message instanceof LeaveRoom) {
			serverMain.leaveRoom(source, ((LeaveRoom) message).getRoomId());
		} else if (message instanceof StartGame) {
			serverMain.startGame(source, (StartGame) message);
		} else if (message instanceof DirectionUpdate) {
			serverMain.positionUpdate(source, (DirectionUpdate) message);
		} else if (message instanceof LoginRequest) {
			serverMain.loginRequest(source, (LoginRequest) message);
		} else if (message instanceof LogoutRequest) {
			serverMain.logoutRequest(source, (LogoutRequest) message);
		}

	}
}
