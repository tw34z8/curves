/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.server;

import java.io.IOException;
import java.util.*;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.network.*;
import com.jme3.network.kernel.KernelException;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;

import curves.common.Log;
import curves.common.network.Game;
import curves.common.network.Objects.GameObject;
import curves.common.network.Objects.Player;
import curves.common.network.Room;
import curves.common.network.SerializeHelper;
import curves.common.network.messages.LobbyMessage;
import curves.common.network.messages.LoginRequest;
import curves.common.network.messages.LoginResponse;
import curves.common.network.messages.LogoutRequest;
import curves.common.network.messages.game.*;
import curves.common.network.messages.room.JoinRoom;
import curves.common.network.messages.room.RoomCreated;
import curves.common.network.messages.room.RoomJoined;
import curves.common.network.messages.room.RoomUpdate;
import java.util.concurrent.CopyOnWriteArrayList;

public class ServerMain extends SimpleApplication implements ConnectionListener {

	public static final int TICKRATE = 100;
	public static final int UPDATERATE = 30; // snapshots/second
	private static final int PORT = 1337;
	public static final String PLAYER_OBJECT_ATTRIBUTE = "PLAYER_OBJECT_ATTRIBUTE";
	public static final String PLAYER_CONNECTION_ID = "PLAYER_CONNECTION_ID";
	private Server serverInstance;
	private ClientListener clientListener = new ClientListener(this);
	private List<Room> rooms = new ArrayList<>();
	private HashMap<Player, HostedConnection> connections = new HashMap<>();
	private HashMap<String, Game> games = new HashMap<>();
	private int playerCount = 1;
	private int roomCount = 1;
	public static AssetManager assetManager;

	public static void main(String[] args) {
		System.out.println("Starting server");
		AppSettings settings = new AppSettings(true);
		settings.setFrameRate(TICKRATE);
		ServerMain app = new ServerMain();
		app.setSettings(settings);
		app.start(JmeContext.Type.Headless);


	}

	public void setRootNode(Node rootNode) {
		this.rootNode = rootNode;
	}

	@Override
	public void simpleInitApp() {
		assetManager = getAssetManager();
		SerializeHelper.register();

		try {
			Log.debug("Starting server onNetwork.createServer( port '%s'", PORT);
			serverInstance = Network.createServer(PORT);
			serverInstance.addMessageListener(clientListener);
			serverInstance.addConnectionListener(this);
			serverInstance.start();
			Log.info("Server started on port '%s'", PORT);
		} catch (KernelException | IOException ex) {
			Log.error("Error while starting server: '%s'", ex);
			ex.printStackTrace();
			stop();
		}
	}

	@Override
	public void connectionAdded(Server server, HostedConnection conn) {
		Log.info("New connection added.");
		ArrayList<Player> players = new ArrayList<>();
		conn.setAttribute(PLAYER_OBJECT_ATTRIBUTE, players);
		conn.setAttribute(PLAYER_CONNECTION_ID, UUID.randomUUID().toString());
	}

	private void broadcastMessage(Message message) {
		for (HostedConnection connection : serverInstance.getConnections()) {
			connection.send(message);
		}
	}

	@Override
	public void connectionRemoved(Server server, HostedConnection conn) {
		ArrayList<Player> players = conn.getAttribute(PLAYER_OBJECT_ATTRIBUTE);
		// Set disconnected flag
		for (Player player : players) {
			player.setDisconnected(true);
			Log.info("User '%s' closed the connection.", player.getUserName());
		}

		// Check if player was in a room
		for (Room room : rooms) {
			for (Player player : room.getPlayers()) {
				for (Player player2 : players) {
					connections.remove(player2);
					player2.setDisconnected(true);
					if (player.equals(player2)) {
						leaveRoom(conn, room.getId());
						return;
					}
				}
			}
		}

		// Check if player was in a game, which is now empty
		for (Object key : games.keySet()) {
			Game game = games.get(key);
			int playersDisconnected = 0;
			// count all disconnected players in room
			for (Player player : game.getPlayers()) {
				if (player.isDisconnected()) {
					playersDisconnected++;
				}
			}
			if (playersDisconnected == game.getPlayers().size()) {
				// remove room because empty
				Log.info("Removing game '%s' whith no more players", game.getGameID());
				games.remove(key);
				game.stopGame();
				return;
			}
		}

	}

	@Override
	public void destroy() {
		serverInstance.close();
		super.destroy();
	}

	public List<Room> getRooms() {
		return rooms;
	}

	/**
	 * Will be called if a room shall be created
	 *
	 * @param source Host who wants to create a room
	 */
	public void createRoom(HostedConnection source) {
		ArrayList<Player> players = source.getAttribute(PLAYER_OBJECT_ATTRIBUTE);
		for (Player player : players) {
			Log.info("User '%s' wants to create a room.", player.getUserName());
		}

		Room room = new Room(roomCount++);

		for (Player player : players) {
			room.addPlayer(player);
		}

		rooms.add(room);
		Log.info("Room created.");
		// inform source of newly created room
		source.send(new RoomCreated(room));
		// inform other clients
		broadcastMessage(new LobbyMessage(rooms));
	}

	/**
	 * Will be called if a client wants to join a room
	 *
	 * @param source Client who wants to join
	 * @param message Room to join
	 */
	public void joinRoom(HostedConnection source, JoinRoom roomToJoin) {
		ArrayList<Player> players = source.getAttribute(PLAYER_OBJECT_ATTRIBUTE);
		for (Player player : players) {
			Log.info("Player '%s' wants to join room '%s'.", player.getUserName(), roomToJoin.getRoom().getId());
		}
		// Search room
		for (Room room : rooms) {
			if (room.getId() == roomToJoin.getRoom().getId()) {
				for (Player player : players) {
					if (player.getID().equals(roomToJoin.getPlayer().getID())) {
						room.addPlayer(player);
						Log.info("Room joined succesfully");
						source.send(new RoomJoined(room));
						// Inform all players in the room
						for (Player player2 : room.getPlayers()) {
							Log.info("Informing player '%s'.", player2.getUserName());

							HostedConnection hostedConnection = connections.get(player2);
							if (hostedConnection != null) {
								hostedConnection.send(new RoomUpdate(room));
							} else {// TODO how can this happen
								Log.warning("Somehow a disconnected player was still in this room");
							}
						}
						// Inform players in lobby
						broadcastMessage(new LobbyMessage(rooms));
						return;
					}
				}
			}
		}
		Log.warning("Room joining failed");
	}

	/**
	 * Will be called if a client leaves a room
	 *
	 * @param source
	 * @param message
	 */
	public void leaveRoom(HostedConnection source, int roomIdToLeave) {
		ArrayList<Player> players = source.getAttribute(PLAYER_OBJECT_ATTRIBUTE);
		Room roomToLeave = getRoomFromId(roomIdToLeave);

		for (Player player : players) {
			Log.info("Player '" + player.getUserName() + "'left room '" + roomToLeave.getId() + "'.");
		}
		try {
			for (Player player : players) {
				Log.info("Removing player '" + player.getUserName() + "'.");
				roomToLeave.removePlayer(player);
				// Inform all players in the room
				for (Player player2 : roomToLeave.getPlayers()) {
					Log.info("Informing player '" + player2.getUserName() + "'.");
					connections.get(player2).send(new RoomUpdate(roomToLeave));
				}
				// Inform players in lobby
				broadcastMessage(new LobbyMessage(rooms));
				return;
			}
			Log.warning("Error occured while leaving a room.");
		} catch (Exception e) {
			if ("Room is empty".equals(e.getMessage())) {
				Log.info("Room is empty, removing it");
				rooms.remove(roomToLeave);
				// Inform players in lobby
				broadcastMessage(new LobbyMessage(rooms));
			} else {
				Log.debug("An exception occured while leaving room: " + e);
			}
		}
	}

	private Room getRoomFromId(int roomId) {
		for (Room room : rooms) {
			if (room.getId() == roomId) {
				return room;
			}
		}
		return null;
	}

	/**
	 * Will be called if a client wants to start a game
	 *
	 * @param source
	 * @param message
	 */
	void startGame(HostedConnection source, StartGame startGame) {
		Room roomToStart = startGame.getRoom();
		Set<HostedConnection> set = new HashSet<>(); // just for comparision, if two players are on one device we only send one snapshot

		Iterator<Room> iter = rooms.iterator();
		while (iter.hasNext()) {
			Room room = iter.next();
			if (room.getId() == roomToStart.getId()) {
				ArrayList<Player> players = room.getPlayers();
				for (int i = 0; i < players.size(); i++) {
					players.get(i).setPosition(new Vector3f((float) 1920 / (float) players.size() * i, (float) 1080 / (float) players.size() * i, 0)); // init
				} // position,
				// // in
				// game.java
				// // to
				String gameID = UUID.randomUUID().toString();
				Log.info("Starting new Game with '" + players.size() + "' players.");
				Game game = new Game(gameID, players, rootNode, this);
				iter.remove();
				games.put(gameID, game);
				rootNode.addControl(game);
				for (Player player2 : room.getPlayers()) {
					if (set.add(connections.get(player2))) { // if we allready send a snapshot the connection is in the hashset
						Log.info("Informing player '" + player2.getUserName() + "'.");
						connections.get(player2).send(new StartGameBroadcast(gameID, players));
					}

				}
				// Inform players in lobby
				broadcastMessage(new LobbyMessage(rooms));
				continue;
			}
		}
	}

	void positionUpdate(HostedConnection source, DirectionUpdate directionUpdate) {
		Game game = games.get(directionUpdate.getGameID());
		if (game != null) {
			//Log.warning("An direction update was received." + directionUpdate.getDirectionUpdateId());
			game.directionUpdate(directionUpdate);
		} else {
			Log.warning("An direction update for a non existing game was received.");
		}
	}

	void sendSnapshot(Game game) {
		ArrayList<Player> players = new ArrayList<>(game.getPlayers());
		Set<HostedConnection> set = new HashSet<>(); // just for comparision, if two players are on one device we only send one snapshot
		ArrayList<GameObject> gameObjects = game.getGameObjects();

		Snapshot snapshot = new Snapshot(gameObjects, game.getTick(), game.getPassedTime());

		for (Player player : players) {
			// Log.debug("Sending snapshot to player '%s'. with ID: '%s'", player.getUserName(), player.getID());
			if (set.add(connections.get(player))) { // if we allready send a snapshot the connection is in the hashset
				if (!player.isDisconnected()) {
					connections.get(player).send(snapshot);
				}
			}
		}

	}

	void loginRequest(HostedConnection source, LoginRequest loginRequest) {
		Player player;
		String PlayerID = UUID.randomUUID().toString();
		if (loginRequest.getUserName() != null && !loginRequest.getUserName().isEmpty()) { // non anonymous login TODO loginmanager
			player = new Player(loginRequest.getUserName(), (String) source.getAttribute(PLAYER_CONNECTION_ID), PlayerID);
			playerCount++;
		} else { // anonymous login
			Log.debug("an additional player on a device will be added ...");
			player = new Player("Player " + playerCount++, (String) source.getAttribute(PLAYER_CONNECTION_ID), PlayerID);
		}
		ArrayList<Player> playersOnDevice = (ArrayList<Player>) source.getAttribute(PLAYER_OBJECT_ATTRIBUTE);
		playersOnDevice.add(player);
		connections.put(player, source);
		source.send(new LoginResponse(player));
	}

	@SuppressWarnings("unchecked")
	public void gameFinished(Game game, Player winner) {
		Set<HostedConnection> set = new HashSet<>(); // just for comparision, if two players are on one device we only send one snapshot
		game.stopGame();
		games.remove(game.getGameID());
		// inform all playing users about the winner
		GameFinishedMessage gameFinishedMessage = new GameFinishedMessage(game.getPlayers(), winner);
		for (Player player : game.getPlayers()) {

			if (player.isDisconnected()) {
				continue;
			}
			HostedConnection hostedConnection = connections.get(player);
			if (set.add(hostedConnection)) { // if we allready send a snapshot the connection is in the hashset
				hostedConnection.send(gameFinishedMessage);
			} else {
				((ArrayList<Player>) connections.get(player).getAttribute(PLAYER_OBJECT_ATTRIBUTE)).remove(player); // remove additional playes on the device
				connections.remove(player);
			}
		}
		for (Player player : game.getPlayers()) {
			player.reset();
		}
	}

	void logoutRequest(HostedConnection source, LogoutRequest logoutRequest) {
		Iterator<Player> iterator = ((ArrayList<Player>) source.getAttribute(PLAYER_OBJECT_ATTRIBUTE)).iterator();
		while (iterator.hasNext()) {
			Player player = iterator.next();
			if (player.getID().equals(logoutRequest.getPlayer().getID())) {
				Log.debug("logging out Player " + player.getUserName() + " with ID " + player.getID() + ".");
				iterator.remove();
				connections.remove(player);
			}
		}
	}
}
