package curves.common;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import curves.common.network.Objects.GameObject;
import curves.common.network.Objects.Player;
import curves.common.network.messages.game.DirectionUpdate;

public class Movement {

	/**
	 * Applies one direction update and simulates a tick
	 *
	 * @param tpf
	 * @param player
	 * @param directionUpdate
	 */
	public static void applyDirectionUpdate(Player player, DirectionUpdate directionUpdate) {
		int direction = directionUpdate.getDirection();
		player.setLastAcknowlegedDirectionUpdateId(directionUpdate.getDirectionUpdateId());
		player.setDirection(direction);
		tick(directionUpdate.getTpf(), player);
	}

	/**
	 * Calculates a tick for the given player
	 *
	 * @param tpf delta since last tick
	 * @param player player to simulate
	 */
	public static void tick(float tpf, GameObject gameObject) {
		float direction = gameObject.getDirection();
		Vector3f oldPosition = gameObject.getPosition();

		Vector3f oldDirection = gameObject.getDirectionVector();
		Vector3f newDirection = oldDirection;

		// TODO maybe as powerUp change speeds of different players
		final float playerSpeedPerSecond = 50f; // 50 pixel / second
		final float revolutionsPerSecond = .25f; // 90degree per second (1/4 circle/s)


		// if direction is not 0, direction vector needs to be updated
		if (direction != 0 && direction != 2) {
			float rotationAmount = (float) (2 * Math.PI / (1 / revolutionsPerSecond) * -direction * tpf);

			Vector2f rotateVector2f = rotateVector2f(new Vector2f(newDirection.x, newDirection.y), rotationAmount);
			newDirection = new Vector3f(rotateVector2f.x, rotateVector2f.y, 0);
		}

		newDirection = newDirection.normalize().mult(playerSpeedPerSecond * tpf);

		Vector3f newPosition = oldPosition.add(newDirection);

		gameObject.setPosition(newPosition);
		gameObject.setDirectionVector(newDirection);
	}

	static Vector2f rotateVector2f(Vector2f v, float degrees) {
		return new Vector2f((float) (v.x * Math.cos(degrees) - v.y * Math.sin(degrees)), (float) (v.x * Math.sin(degrees) + v.y * Math.cos(degrees)));
	}
}
