/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network;

import curves.common.network.Objects.Player;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

import curves.common.Log;
import curves.common.Movement;
import static curves.common.Movement.applyDirectionUpdate;
import curves.common.network.Objects.Bullet;
import curves.common.network.Objects.Explosion;
import curves.common.network.Objects.GameObject;
import curves.common.network.messages.game.DirectionUpdate;
import curves.server.ServerMain;
import curves.server.SnapshotUpdater;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author db-141205
 */
public class Game extends AbstractControl {

	private double tick = 0; //hier hast dein tick
	private double passedTime;
	long lastTime = System.nanoTime();
	private SnapshotUpdater snapShotupdater;
	private ServerMain serverMain;
	private ArrayList<Player> players = new ArrayList<>(); // list of players in this game
	private CopyOnWriteArrayList<Bullet> bullets = new CopyOnWriteArrayList<>();
	private CopyOnWriteArrayList<Explosion> explosions = new CopyOnWriteArrayList<>();
	private String gameID;
	private HashMap<Player, ArrayList<Vector3f>> routes = new HashMap<>(); // routes of all players
	private HashMap<Player, CopyOnWriteArrayList<DirectionUpdate>> directionUpdatesMap = new HashMap<>(); // puffer for directionUpdates
	private double game[][] = new double[1920][1080];

	public Game(String gameID, ArrayList<Player> players, Node rootNode, ServerMain serverMain) {
		snapShotupdater = new SnapshotUpdater(this, serverMain);
		this.players = players;
		this.gameID = gameID;
		this.serverMain = serverMain;

		int i = 0;
		for (Player player : players) {
			routes.put(player, new ArrayList<Vector3f>());
			routes.get(player).add(new Vector3f((float) 1920 / (float) players.size() * i, (float) 1080 / (float) players.size() * i, 0));
			directionUpdatesMap.put(player, new CopyOnWriteArrayList<DirectionUpdate>());
			i++;
		}
	}

	/**
	 * @return the players
	 */
	public ArrayList<Player> getPlayers() {
		return players;
	}

	/**
	 * @return the gameID
	 */
	public String getGameID() {
		return gameID;
	}

	@Override
	protected void controlUpdate(float tpf) {
		Iterator<Player> iter = getPlayers().iterator();
		Iterator<Bullet> iter2 = bullets.iterator();
		while (iter2.hasNext()) {
			Bullet bullet = iter2.next();
			Movement.tick(tpf * 30, bullet);

			if (bullet.getPosition().x >= 1920 - 16 || bullet.getPosition().x <= 0 + 16 || bullet.getPosition().y >= 1080 - 16 || bullet.getPosition().y <= 0 + 16) {
				bullets.remove(bullet);
				explosions.add(new Explosion(bullet.getConnectionID(), UUID.randomUUID().toString(), bullet.getPosition()));
			} else {
				Vector3f dummy = drawCircleShoot(Math.round(bullet.getPosition().x), Math.round(bullet.getPosition().y), 15);
				if (dummy.length() != 0.0) {
					bullets.remove(bullet);
					explosions.add(new Explosion(bullet.getConnectionID(), UUID.randomUUID().toString(), dummy));
				}
			}
		}
		// Update all player positions
		while (iter.hasNext()) {
			Player player = iter.next();
			if (player.isDead()) {
				continue;
			}
			CopyOnWriteArrayList<DirectionUpdate> directionUpdates = directionUpdatesMap.get(player);

			if (!directionUpdates.isEmpty()) {

				int size = directionUpdates.size();
				//Log.debug("size: " + size);
				DirectionUpdate directionUpdate;
				for (int i = 0; i < size; i++) {
					directionUpdate = directionUpdates.get(0);
					if (directionUpdate.getDirection() == 2 && player.getCooldown() > 0.6f) {
						player.setCooldown(0.0f);
						Bullet bullet = new Bullet(player.getConnectionID(), UUID.randomUUID().toString(), player.getPosition().clone(), player.getDirectionVector().clone());
						bullets.add(bullet);
						Movement.tick(tpf * 30, bullet);
						Movement.tick(tpf * 30, bullet);
						Movement.tick(tpf * 30, bullet);
					}
					player.setCooldown(directionUpdate.getTpf() + player.getCooldown());
					applyDirectionUpdate(player, directionUpdate);
					directionUpdates.remove(directionUpdate);
				}

			}
			routes.get(player).add(player.getPosition());
		}
		intersectionTest();
		checkGameOver();
		tick++; // 1-tick = 1-loop oke? das verwirrt mich wenn du hier mit nanosekunden hantierst und den scheiß ticks nennst
		passedTime += tpf * 1000000000d;

	}

	private void checkGameOver() {
		// Check if game is over and we have a winner
		int playersAlive = 0;
		Player winner = null;
		for (Player player : players) {
			if (!player.isDead() && !player.isDisconnected()) {
				winner = player;
				playersAlive++;
				if (playersAlive > 1) {
					return; // if we have more than two playing users, game is not over
				}
			}
		}

		if (playersAlive == 1) {
			Log.info("We have a winner: '%s'", winner.getUserName());
			serverMain.gameFinished(this, winner);
		} else if (playersAlive <= 0) {
			// dont know if this happens
			Log.error("No more players are alive! DRAW");
		}
	}

	public void directionUpdate(DirectionUpdate directionUpdate) {
		Player player2 = getPlayer(directionUpdate.getPlayerID());
		directionUpdatesMap.get(player2).add(directionUpdate);
	}

	public void stopGame() {
		if (!snapShotupdater.isInterrupted()) {
			snapShotupdater.interrupt();
		}
		serverMain.getRootNode().removeControl(this);
	}

	/**
	 * @return the routes
	 */
	public HashMap<Player, ArrayList<Vector3f>> getRoutes() {
		return routes;
	}

	private void intersectionTest() {
		Vector3f[] fatLine = new Vector3f[27];
		for (Player player : getPlayers()) {
			if (player.isDead()) {
				continue;
			}
			Vector3f[] intersectionVector = new Vector3f[2];
			ArrayList<Vector3f> arrayList = getRoutes().get(player);
			int size = arrayList.size();
			if (size < 2) {
				return;
			}
			intersectionVector[0] = arrayList.get(size - 2);
			intersectionVector[1] = arrayList.get(size - 1);
			if (intersectionVector[1].x > 1920 || intersectionVector[1].x < 0 || intersectionVector[1].y > 1080 || intersectionVector[1].y < 0) {
				player.setDead(true);
				increasePointsOfAllLiving();
			}
			float angle = 0;
			float rate = FastMath.TWO_PI / 24;
			for (int i = 0; i < 27; i++) {
				if (i % 3 == 0) {
					fatLine[i] = intersectionVector[1];
				} else {
					fatLine[i] = new Vector3f(FastMath.cos(angle), FastMath.sin(angle), 0.0f).mult(5).add(intersectionVector[1]);
				}
				angle += rate;
			}
			/*for (int i = 0; i < 26; i++) {
			 line2(Math.round(fatLine[i].x), Math.round(fatLine[i].y), Math.round(fatLine[i + 1].x), Math.round(fatLine[i + 1].y), player);
			 }*/
			drawCircle(Math.round(intersectionVector[1].x), Math.round(intersectionVector[1].y), 5, player);
		}
	}

	private Vector3f drawCircleShoot(final int centerX, final int centerY, int radius) {
		boolean returnValue = false;
		int cx = 0, cy = 0;
		for (int y = -radius; y <= radius; y++) {
			for (int x = -radius; x <= radius; x++) {
				if (x * x + y * y <= radius * radius) {
					if (game[centerX + x][centerY + y] != 0) {
						returnValue = true;
						cx = centerX + x;
						cy = centerY + y;
					}
					//game[centerX + x][centerY + y] = 0;
				}
			}
		}

		if (returnValue) {
			radius *= 3;
			for (int y = -radius; y <= radius; y++) {
				for (int x = -radius; x <= radius; x++) {
					if (x * x + y * y <= radius * radius) {
						if (game[cx + x][cy + y] != 0) {
							returnValue = true;
						}
						game[cx + x][cy + y] = 0;
					}
				}
			}

		}
		return new Vector3f(cx, cy, 0);

	}

	private void drawCircle(final int centerX, final int centerY, final int radius, Player player) {
		int d = (5 - radius * 4) / 4;
		int x = 0;
		int y = radius;

		do {
			//line2(centerX + x, centerY + y, circleColor);
			//line2(centerX + x, centerY - y, circleColor);
			line2(centerX - y, centerY + x, centerX + y, centerY + x, player);

			//line2(centerX - x, centerY + y, circleColor);
			//line2(centerX - x, centerY - y, circleColor);
			line2(centerX - y, centerY - x, centerX + y, centerY - x, player);

			//line2(centerX + y, centerY + x, circleColor);
			//line2(centerX + y, centerY - x, circleColor);
			line2(centerX - x, centerY + y, centerX + x, centerY + y, player);

			//line2(centerX - y, centerY + x, circleColor);
			//line2(centerX - y, centerY - x, circleColor);
			line2(centerX - x, centerY - y, centerX + x, centerY - y, player);
			if (d < 0) {
				d += 2 * x + 1;
			} else {
				d += 2 * (x - y) + 1;
				y--;
			}
			x++;
		} while (x <= y);

	}

	/**
	 * modified bresenham line algorithm for intersection testing
	 *
	 * @
	 *
	 * @param x position in frame before in x
	 * @param y position in frame before in y
	 * @param x2 actual position in x
	 * @param y2 actual position y
	 * @param player
	 * @see
	 * https://stackoverflow.com/questions/5186939/algorithm-for-drawing-a-4-connected-line
	 * @see
	 * https://stackoverflow.com/questions/13542925/line-rasterization-4-connected-bresenham
	 */
	public boolean lineShoot(int x0, int y0, int x1, int y1) {
		boolean returnValue = false;
		int dx = Math.abs(x1 - x0);
		int dy = Math.abs(y1 - y0);
		int sgnX = x0 < x1 ? 1 : -1;
		int sgnY = y0 < y1 ? 1 : -1;
		int e = 0;
		for (int i = 0; i < dx + dy; i++) {
			try {
				if (game[x0][y0] != 0 && game[x0][y0] <= tick - 80) {
					Log.debug("x: " + x0 + " y: " + y0 + " value: " + game[x0][y0] + " acutal tick: " + tick);
					returnValue |= true;
					//increasePointsOfAllLiving();
				}
				game[x0][y0] = 0;

			} catch (Exception exc) {
			}
			int e1 = e + dy;
			int e2 = e - dx;
			if (Math.abs(e1) < Math.abs(e2)) {
				x0 += sgnX;
				e = e1;
			} else {
				y0 += sgnY;
				e = e2;
			}
		}
		return returnValue;
	}

	/**
	 * modified bresenham line algorithm for intersection testing
	 *
	 * @
	 *
	 * @param x position in frame before in x
	 * @param y position in frame before in y
	 * @param x2 actual position in x
	 * @param y2 actual position y
	 * @param player
	 * @see
	 * https://stackoverflow.com/questions/5186939/algorithm-for-drawing-a-4-connected-line
	 * @see
	 * https://stackoverflow.com/questions/13542925/line-rasterization-4-connected-bresenham
	 */
	public void line2(int x0, int y0, int x1, int y1, Player player) {
		int dx = Math.abs(x1 - x0);
		int dy = Math.abs(y1 - y0);
		int sgnX = x0 < x1 ? 1 : -1;
		int sgnY = y0 < y1 ? 1 : -1;
		int e = 0;
		for (int i = 0; i < dx + dy; i++) {
			try {
				if (game[x0][y0] != 0 && game[x0][y0] != tick && game[x0][y0] <= tick - 51) {
					Log.debug("x: " + x0 + " y: " + y0 + " value: " + game[x0][y0] + " acutal tick: " + tick);
					player.setDead(true);
					increasePointsOfAllLiving();
				}
				game[x0][y0] = tick;

			} catch (Exception exc) {
			}
			int e1 = e + dy;
			int e2 = e - dx;
			if (Math.abs(e1) < Math.abs(e2)) {
				x0 += sgnX;
				e = e1;
			} else {
				y0 += sgnY;
				e = e2;
			}
		}
	}

	/**
	 * modified bresenham line algorithm for intersection testing
	 *
	 * @param x position in frame before in x
	 * @param y position in frame before in y
	 * @param x2 actual position in x
	 * @param y2 actual position y
	 * @param player
	 *
	 */
	@Deprecated
	public void line(int x, int y, int x2, int y2, Player player) {
		int w = x2 - x;
		int h = y2 - y;
		int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
		if (w < 0) {
			dx1 = -1;
		} else if (w > 0) {
			dx1 = 1;
		}
		if (h < 0) {
			dy1 = -1;
		} else if (h > 0) {
			dy1 = 1;
		}
		if (w < 0) {
			dx2 = -1;
		} else if (w > 0) {
			dx2 = 1;
		}
		int longest = Math.abs(w);
		int shortest = Math.abs(h);
		if (!(longest > shortest)) {
			longest = Math.abs(h);
			shortest = Math.abs(w);
			if (h < 0) {
				dy2 = -1;
			} else if (h > 0) {
				dy2 = 1;
			}
			dx2 = 0;
		}
		int numerator = longest >> 1;
		for (int i = 0; i <= longest; i++) {
			try {
				if (game[x][y] != 0 && game[x][y] != tick && game[x][y] <= tick - 50) {
					Log.debug("x: " + x + " y: " + y + " value: " + game[x][y] + " acutal tick: " + tick);
					player.setDead(true);
					increasePointsOfAllLiving();
				} else {
					game[x][y] = tick;
				}
			} catch (Exception e) {
			}
			numerator += shortest;
			if (!(numerator < longest)) {
				numerator -= longest;
				x += dx1;
				y += dy1;
			} else {
				x += dx2;
				y += dy2;
			}

		}
	}

	private void increasePointsOfAllLiving() {
		for (Player player : getPlayers()) {
			if (player.isDead() || player.isDisconnected()) {
				continue;
			}
			player.increasePoints();

		}
	}

	private Player getPlayer(String playerID) {
		for (Player player : players) {
			if (player.getID().equals(playerID)) {
				return player;
			}
		}
		return null;
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {
		// TODO Auto-generated method stub
	}

	/**
	 * @return the tick
	 */
	public double getTick() {
		return tick;
	}

	/**
	 * @return the passedTime
	 */
	public double getPassedTime() {
		return passedTime;
	}

	public ArrayList<GameObject> getGameObjects() {
		ArrayList<GameObject> gameObjects = new ArrayList<>();
		//Bundle players
		for (Player player : players) {
			gameObjects.add(player);
		}
		//Bundle bullets
		for (Bullet bullet : bullets) {
			gameObjects.add(bullet);
		}
		//Bundle explosions
		for (Explosion explosion : explosions) {
			gameObjects.add(explosion);
			explosions.remove(explosion);
		}
		return gameObjects;
	}
}
