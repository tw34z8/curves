/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import curves.common.network.Objects.Player;

/**
 *
 * @author io
 */
@Serializable
public class LoginResponse extends AbstractMessage {

        private Player player;
        private boolean success = true;
        private String errorMessage;

        public LoginResponse() {
        }

        public LoginResponse(Player player) {
                this.player = player;
                errorMessage = "Successfully logged in.";
        }

        /**
         * @return the success
         */
        public boolean isSuccess() {
                return success;
        }

        /**
         * @return the msg
         */
        public String getMsg() {
                return errorMessage;
        }

        /**
         * @return the player
         */
        public Player getPlayer() {
                return player;
        }
}
