/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

import curves.common.network.Objects.Player;

/**
 *
 * @author io
 */
@Serializable
public class LogoutRequest extends AbstractMessage {

	private Player player;

	public LogoutRequest() {
	}

	public LogoutRequest(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}
}
