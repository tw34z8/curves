package curves.common.network.messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Client requests all Rooms
 *
 * @author Death
 *
 */
@Serializable
public class LoginRequest extends AbstractMessage {
	private String userName;

	public LoginRequest() {
            
	}

	public LoginRequest(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}
}
