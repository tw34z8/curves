package curves.common.network.messages;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Client requests all Rooms
 *
 * @author Death
 *
 */
@Serializable
public class LobbyRequestMessage extends AbstractMessage {

}
