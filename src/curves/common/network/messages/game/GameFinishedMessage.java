/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.messages.game;

import java.util.List;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

import curves.common.network.Objects.Player;

/**
 * This Message is send to all players of a game, when the game is finished
 *
 * @author Death
 *
 */
@Serializable
public class GameFinishedMessage extends AbstractMessage {
	private Player winner;
	private List<Player> players;

	public GameFinishedMessage() {
	}

	public GameFinishedMessage(List<Player> players, Player winner) {
		this.players = players;
		this.winner = winner;
	}

	public Player getWinner() {
		return winner;
	}

	public List<Player> getPlayers() {
		return players;
	}
}
