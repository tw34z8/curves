/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.messages.game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import curves.common.network.Game;
import curves.common.network.Objects.Player;
import java.util.ArrayList;

/**
 * this Message is send from the server to all players withing one room (even the sender who was originally sending the startgame message)
 * Simple Message to inform every player that the game started
 * @author db-141205
 * 
 */

@Serializable
public class StartGameBroadcast extends AbstractMessage {
    private String gameID;
    private ArrayList<Player> players = new ArrayList<>();
    
    public StartGameBroadcast(){}

    public StartGameBroadcast(String gameID, ArrayList<Player> players) {
        this.gameID = gameID;
        this.players = players;
    }

    /**
     * @return the gameID
     */
    public String getGameID() {
        return gameID;
    }

    /**
     * @return the players
     */
    public ArrayList<Player> getPlayers() {
        return players;
    }

}
