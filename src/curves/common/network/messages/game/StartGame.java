/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.messages.game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import curves.common.network.Room;

/**
 * This Message is send by one client, it couses the server to start the game
 * @author db-141205
 */
@Serializable
public class StartGame extends AbstractMessage {
    private Room room;

    public StartGame(){}
    
    public StartGame(Room room){
        this.room = room;
    }
    /**
     * @return the room
     */
    public Room getRoom() {
        return room;
    }

    /**
     * @param room the room to set
     */
    public void setRoom(Room room) {
        this.room = room;
    }
}
