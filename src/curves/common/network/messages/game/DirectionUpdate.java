/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.messages.game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

import curves.common.network.Objects.Player;

/**
 *
 * @author db-141205
 */
@Serializable
public class DirectionUpdate extends AbstractMessage {

	private int direction; // -1 left pressed,0 forward, nothing pressed, 1 right pressed
	private String gameID;
	private String playerID;
	private int directionUpdateId;
	private float tpf;

	public DirectionUpdate() {
	}

	public DirectionUpdate(String playerID, String gameID, int direction, int directionUpdateId, float tpf) {
		this.direction = direction;
		this.gameID = gameID;
		this.playerID = playerID;
		this.directionUpdateId = directionUpdateId;
		this.tpf = tpf;
	}

	/**
	 * @return the direction
	 */
	public int getDirection() {
		return direction;
	}

	/**
	 * @return the gameID
	 */
	public String getGameID() {
		return gameID;
	}

	public String getPlayerID() {
		return playerID;
	}

	public int getDirectionUpdateId() {
		return directionUpdateId;
	}

	/**
	 * @return the tpf
	 */
	public float getTpf() {
		return tpf;
	}

	/**
	 * @param tpf the tpf to set
	 */
	public void setTpf(float tpf) {
		this.tpf = tpf;
	}
}
