/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.messages.game;

import java.util.ArrayList;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import curves.common.network.Objects.GameObject;

import curves.common.network.Objects.Player;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This type of message is broadcasted in an fixed interval from the server.
 *
 * @author io
 */
@Serializable
public class Snapshot extends AbstractMessage {

	private ArrayList<GameObject> gameObjects;
	private double time;
	private double tick;

	public Snapshot() {
	}

	public Snapshot(ArrayList<GameObject> gameObjects, double tick, double passedTime) {
		this.gameObjects = gameObjects;
		this.time = passedTime;
		this.tick = tick;
	}

	/**
	 * @return the playerPosititions
	 */
	public ArrayList<GameObject> getGameObjects() {
		return gameObjects;
	}

	/**
	 * @return the timestamp
	 */
	public double getTime() {
		return time;
	}

	/**
	 * @return the tick
	 */
	public double getTick() {
		return tick;
	}

	/**
	 * @param tick the tick to set
	 */
	public void setTick(double tick) {
		this.tick = tick;
	}
}
