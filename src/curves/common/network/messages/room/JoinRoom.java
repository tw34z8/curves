package curves.common.network.messages.room;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import curves.common.network.Objects.Player;

import curves.common.network.Room;

/**
 * Message requests to create a new room
 *
 * @author Death
 *
 */
@Serializable
public class JoinRoom extends AbstractMessage {

        Room room;
        private Player player;

        public JoinRoom() {
        }

        public JoinRoom(Room room, Player player) {
                this.room = room;
                this.player = player;
        }

        public Room getRoom() {
                return room;
        }

        /**
         * @return the player
         */
        public Player getPlayer() {
                return player;
        }
}
