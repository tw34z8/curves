package curves.common.network.messages.room;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

import curves.common.network.Room;

/**
 * A new room was created
 *
 * @author Death
 *
 */
@Serializable
public class RoomCreated extends AbstractMessage {
	Room room;

	public RoomCreated() {
	}

	public RoomCreated(Room room) {
		this.room = room;
	}

	public Room getRoom() {
		return room;
	}
}
