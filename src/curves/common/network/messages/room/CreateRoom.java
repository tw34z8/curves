package curves.common.network.messages.room;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Message requests to create a new room
 *
 * @author Death
 *
 */
@Serializable
public class CreateRoom extends AbstractMessage {
}
