package curves.common.network.messages.room;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * Message requests to create a new room
 *
 * @author Death
 *
 */
@Serializable
public class LeaveRoom extends AbstractMessage {
	int roomId;

	public LeaveRoom() {
	}

	public LeaveRoom(int roomId) {
		this.roomId = roomId;
	}

	public int getRoomId() {
		return roomId;
	}

}
