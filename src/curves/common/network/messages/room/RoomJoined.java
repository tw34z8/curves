package curves.common.network.messages.room;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

import curves.common.network.Room;

/**
 * Message requests to create a new room
 *
 * @author Death
 *
 */
@Serializable
public class RoomJoined extends AbstractMessage {
	Room room;

	public RoomJoined() {
	}

	public RoomJoined(Room room) {
		this.room = room;
	}

	public Room getRoom() {
		return room;
	}

}
