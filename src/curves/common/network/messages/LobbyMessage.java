package curves.common.network.messages;

import java.util.List;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

import curves.common.network.Room;

@Serializable
public class LobbyMessage extends AbstractMessage {
	private List<Room> rooms;

	public LobbyMessage() {
	}

	public LobbyMessage(List<Room> rooms) {
		this.rooms = rooms;
	}

	public List<Room> getRooms() {
		return rooms;
	}

}
