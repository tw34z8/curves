/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.Objects;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author db-141205
 */
@Serializable
public class Bullet extends GameObject {

	public Bullet() {
	}

	public Bullet(String connectionID, String ID, Vector3f position, Vector3f dircetion) {
		super(connectionID, ID);
		super.setPosition(position);
		super.setDirectionVector(dircetion);
	}
}
