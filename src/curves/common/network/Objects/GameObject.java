/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.Objects;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author db-141205
 */
@Serializable
public abstract class GameObject {

	private int direction;
	private Vector3f position;
	private Vector3f directionVector;
	private String connectionID;
	private String ID;

	public GameObject() {
	}

	public GameObject(String connectionID, String ID) {
		this.connectionID = connectionID;
		this.ID = ID;
	}

	/**
	 * @return the position
	 */
	public Vector3f getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Vector3f position) {
		this.position = position;
	}

	/**
	 * @return the connectionID
	 */
	public String getConnectionID() {
		return connectionID;
	}

	/**
	 * @return the ID
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @return the directionVector
	 */
	public Vector3f getDirectionVector() {
		return directionVector;
	}

	/**
	 * @param directionVector the directionVector to set
	 */
	public void setDirectionVector(Vector3f directionVector) {
		this.directionVector = directionVector;
	}

	/**
	 * @return the direction
	 */
	public int getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}
}
