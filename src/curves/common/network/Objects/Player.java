/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.Objects;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import curves.common.network.Objects.GameObject;

@Serializable
public class Player extends GameObject {

	private float cooldown;
	private boolean key_left = false;
	private boolean key_right = false;
	private boolean dead;
	private boolean disconnected;
	private String userName;
	private ColorRGBA color;
	private int points;
	private int lastAcknowlegedDirectionUpdateId = 0;

	public Player() {
	}

	public Player(String name, String connectionID, String ID) {
		super(connectionID, ID);
		super.setPosition(new Vector3f());
		super.setDirectionVector(new Vector3f(1, 0, 0));
		color = new ColorRGBA(ColorRGBA.Cyan);
		userName = name;
		dead = false;
		disconnected = false;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return getUserName();
	}

	/**
	 * @return the color
	 */
	public ColorRGBA getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(ColorRGBA color) {
		this.color = color;
	}

	/**
	 * @return the isDead
	 */
	public boolean isDead() {
		return dead;
	}

	/**
	 * @param isDead the isDead to set
	 */
	public void setDead(boolean isDead) {
		dead = isDead;
	}

	public boolean isDisconnected() {
		return disconnected;
	}

	public void setDisconnected(boolean disconnected) {
		this.disconnected = disconnected;
	}

	public void reset() {
		super.setDirection(0);
		dead = false;
		points = 0;
		setPosition(new Vector3f());
		setDirectionVector(new Vector3f(1, 0, 0));
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void increasePoints() {
		points++;
	}

	public void setLastAcknowlegedDirectionUpdateId(int directionUpdateId) {
		lastAcknowlegedDirectionUpdateId = directionUpdateId;
	}

	public int getLastAcknowlegedDirectionUpdateId() {
		return lastAcknowlegedDirectionUpdateId;
	}

	/**
	 * @return the key_left
	 */
	public boolean isKey_left() {
		return key_left;
	}

	/**
	 * @param key_left the key_left to set
	 */
	public void setKey_left(boolean key_left) {
		this.key_left = key_left;
	}

	/**
	 * @return the key_right
	 */
	public boolean isKey_right() {
		return key_right;
	}

	/**
	 * @param key_right the key_right to set
	 */
	public void setKey_right(boolean key_right) {
		this.key_right = key_right;
	}

	/**
	 * @return the cooldown
	 */
	public float getCooldown() {
		return cooldown;
	}

	/**
	 * @param cooldown the cooldown to set
	 */
	public void setCooldown(float cooldown) {
		this.cooldown = cooldown;
	}
}
