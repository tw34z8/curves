/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package curves.common.network.Objects;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author io
 */
@Serializable
public class Explosion extends GameObject {

	public Explosion() {
	}

	public Explosion(String ConnectionID, String ID, Vector3f position) {
		super(ConnectionID, ID);
		super.setPosition(position);
		super.setDirectionVector(new Vector3f());
	}
}
