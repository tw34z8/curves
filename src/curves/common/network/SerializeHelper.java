package curves.common.network;

import curves.common.network.Objects.Player;
import com.jme3.network.serializing.Serializer;

import curves.common.Log;
import curves.common.network.Objects.Bullet;
import curves.common.network.Objects.Explosion;
import curves.common.network.Objects.GameObject;
import curves.common.network.messages.LobbyMessage;
import curves.common.network.messages.LobbyRequestMessage;
import curves.common.network.messages.LoginRequest;
import curves.common.network.messages.LoginResponse;
import curves.common.network.messages.LogoutRequest;
import curves.common.network.messages.game.*;
import curves.common.network.messages.room.*;

public class SerializeHelper {

	private static Class[] registeredMessageClasses = {LobbyMessage.class, LobbyRequestMessage.class, Room.class, Player.class, CreateRoom.class,
		RoomCreated.class, JoinRoom.class, LeaveRoom.class, RoomJoined.class, RoomUpdate.class, StartGame.class, DirectionUpdate.class,
		StartGameBroadcast.class, Snapshot.class, LoginRequest.class, LoginResponse.class, GameFinishedMessage.class, LogoutRequest.class, Bullet.class, GameObject.class, Explosion.class};

	public static void register() {
		Log.info("Registering '%s' classes at Serializer.", registeredMessageClasses.length);
		Serializer.registerClasses(registeredMessageClasses);
	}
}
