package curves.common.network;

import curves.common.network.Objects.Player;
import com.jme3.math.ColorRGBA;
import java.util.ArrayList;

import com.jme3.network.serializing.Serializable;
import java.util.HashMap;

@Serializable
public class Room {
	private static final int MAX_PLAYER_COUNT = 4;
	private ArrayList<Player> players = new ArrayList<Player>();
        private ArrayList<ColorRGBA> colors = new ArrayList<ColorRGBA>(){{
                       add(ColorRGBA.Blue);
            add(ColorRGBA.Green);
            add(ColorRGBA.Yellow);
            add(ColorRGBA.Red);
        }};
        private HashMap<Player, ColorRGBA> colorMap= new HashMap<>();
	private int playerCount = 0;
	private int id;

	public Room() {
	}

        
        public Room(String name){
        
        }
        
	public Room(int id) {
		this.id = id;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public int getId() {
		return id;
	}

	public void addPlayer(Player player) {
                for(ColorRGBA color : colors){
                    if(!colorMap.containsValue(color)){
                        colorMap.put(player, color);
                        player.setColor(color);
                    }
                }
		if (playerCount >= MAX_PLAYER_COUNT) {
			throw new RuntimeException("Room exceeded maxium");
		}
		players.add(player);
		playerCount++;
	}

	public void removePlayer(Player player) throws Exception {
                colorMap.remove(player);
		players.remove(player);
		// remove player from room
		playerCount--;
		if (playerCount <= 0)
			throw new Exception("Room is empty");
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public int getMaxPlayers() {
		return MAX_PLAYER_COUNT;
	}
}
