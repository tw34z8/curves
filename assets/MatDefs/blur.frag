

precision lowp float;
precision lowp int;

uniform sampler2D m_Texture;
uniform vec2 m_uShift;

varying  vec2 texCoord;

void main(){
    //returning the color of the pixel (here solid blue)
    //- gl_FragColor is the standard GLSL variable that holds the pixel
    //color. It must be filled in the Fragment Shader.
    //http://www.colorado.edu/physics/phet/dev/molecule-shapes/test/a/Common/MatDefs/Blur/

    //hoizontal
    vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
    //vertikal
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 7.0*m_uShift.y)) * 0.0044299121055113265;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 6.0*m_uShift.y)) * 0.00895781211794;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 5.0*m_uShift.y)) * 0.0215963866053;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 4.0*m_uShift.y)) * 0.0443683338718;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 3.0*m_uShift.y)) * 0.0776744219933;
    color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 2.0*m_uShift.y)) * 0.12;
    color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 1.0*m_uShift.y)) * 0.15;
    color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y - 0.0*m_uShift.y)) * 0.16;
    color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y + 1.0*m_uShift.y)) * 0.15;
    color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y + 2.0*m_uShift.y)) * 0.12;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y + 3.0*m_uShift.y)) * 0.0776744219933;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y + 4.0*m_uShift.y)) * 0.0443683338718;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y + 5.0*m_uShift.y)) * 0.0215963866053;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y + 6.0*m_uShift.y)) * 0.00895781211794;
    //color += texture2D(m_Texture, vec2(texCoord.x, texCoord.y + 7.0*m_uShift.y)) * 0.0044299121055113265;

    //horizontal
    //color += texture2D(m_Texture, vec2(texCoord.x - 7.0*m_uShift.x, texCoord.y)) * 0.0044299121055113265;
    //color += texture2D(m_Texture, vec2(texCoord.x - 6.0*m_uShift.x, texCoord.y)) * 0.00895781211794;
    //color += texture2D(m_Texture, vec2(texCoord.x - 5.0*m_uShift.x, texCoord.y)) * 0.0215963866053;
    //color += texture2D(m_Texture, vec2(texCoord.x - 4.0*m_uShift.x, texCoord.y)) * 0.0443683338718;
    //color += texture2D(m_Texture, vec2(texCoord.x - 3.0*m_uShift.x, texCoord.y)) * 0.0776744219933;
    color += texture2D(m_Texture, vec2(texCoord.x - 2.0*m_uShift.x, texCoord.y)) * 0.12;
    color += texture2D(m_Texture, vec2(texCoord.x - 1.0*m_uShift.x, texCoord.y)) * 0.15;
    color += texture2D(m_Texture, vec2(texCoord.x - 0.0*m_uShift.x, texCoord.y)) * 0.16;
    color += texture2D(m_Texture, vec2(texCoord.x + 1.0*m_uShift.x, texCoord.y)) * 0.15;
    color += texture2D(m_Texture, vec2(texCoord.x + 2.0*m_uShift.x, texCoord.y)) * 0.12;
    //color += texture2D(m_Texture, vec2(texCoord.x + 3.0*m_uShift.x, texCoord.y)) * 0.0776744219933;
    //color += texture2D(m_Texture, vec2(texCoord.x + 4.0*m_uShift.x, texCoord.y)) * 0.0443683338718;
    //color += texture2D(m_Texture, vec2(texCoord.x + 5.0*m_uShift.x, texCoord.y)) * 0.0215963866053;
    //color += texture2D(m_Texture, vec2(texCoord.x + 6.0*m_uShift.x, texCoord.y)) * 0.00895781211794;
    //color += texture2D(m_Texture, vec2(texCoord.x + 7.0*m_uShift.x, texCoord.y)) * 0.0044299121055113265;


    gl_FragColor = color;
}