
precision lowp float;
precision lowp int;
//the global uniform World view projection matrix
//(more on global uniforms below)

uniform mat4 g_WorldViewProjectionMatrix;
attribute vec2 inTexCoord;
attribute vec4 inPosition;
varying vec2 texCoord;
void main(){
    texCoord = inTexCoord;
    gl_Position = g_WorldViewProjectionMatrix * inPosition;
}