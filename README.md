###JMonkeyEngine###
* tonegodGUI: http://hub.jmonkeyengine.org/wiki/doku.php/jme3:contributions:tonegodgui
* Package-Hierarchy einstellen: ![tree.png](https://bitbucket.org/repo/BdrzBb/images/1369203352-tree.png)


###Networking###
* Source Multiplayer Networking: https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking
* Source Latency Compensating Methods: https://developer.valvesoftware.com/wiki/Latency_Compensating_Methods_in_Client/Server_In-game_Protocol_Design_and_Optimization
* Networking: How a Shooter Shoots: http://immersedcode.org/2011/12/16/how-a-shooter-shoots/
